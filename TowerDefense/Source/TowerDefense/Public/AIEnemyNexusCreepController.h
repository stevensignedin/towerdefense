// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AICreepController.h"
#include "NexusTurret.h"
#include "EnemyNexusCreepCharacter.h"
#include "TowerDefenseCharacter.h"
#include "AIEnemyNexusCreepController.generated.h"

UCLASS()
class TOWERDEFENSE_API AAIEnemyNexusCreepController : public AAICreepController
{
	GENERATED_UCLASS_BODY()

	//	Immediately go for the nexus
	void BeginPlay() override;

	void Tick(float Seconds) override;

	//	Attack once reached nexus
	void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;

protected:

	//	Find the game nexus
	ANexusTurret* getNexus();
};
