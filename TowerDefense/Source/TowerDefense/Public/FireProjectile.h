// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TowerProjectile.h"
#include "FireProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AFireProjectile : public ATowerProjectile
{
	GENERATED_UCLASS_BODY()

	void onActorBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;
};
