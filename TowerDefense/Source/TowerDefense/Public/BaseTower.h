// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Rock.h"
#include "HealthBar.h"
#include "TowerProjectile.h"
#include "GameFramework/Pawn.h"
#include "BaseTower.generated.h"

/**
*
*/
UCLASS()
class TOWERDEFENSE_API ABaseTower : public APawn
{
	GENERATED_UCLASS_BODY()

	//	Cost to build this tower
	int32 cost = 100; 

	//	The closest another tower can be to this one
	static int32 minDistance;

	//	Tower's personal ID
	int32 ID;

	//	Boolean that tells you if the tower is open or not
	bool alive;

	//	Tells if tower is unreachable (FYI for creep controllers)
	bool reachable;

	//	Tower's current camera view
	FTViewTarget myVT;

	//	Health bar
	AHealthBar* myHealthBar;
	UPROPERTY(EditAnywhere, Category = Health)
		TSubclassOf<class AHealthBar> healthBarClass;

	//	Array of different health bars
	TArray<UClass*> barTypes;
	float baseHealth;

	UPROPERTY(EditAnywhere, Category = Health)
		float Health;

	UPROPERTY(EditAnywhere, Category = Attack)
		float attackSpeed;

	//  Tower's body
	UPROPERTY(VisibleDefaultsOnly, Category = Structure)
		TSubobjectPtr<class UStaticMeshComponent> towerMesh;

	//  Rubble to spawn upon tower destruction
	UPROPERTY(EditAnywhere, Category = Structure)
		TSubclassOf<class AObstacle> rubbleToSpawn;

	//  Tower's camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Structure)
		TSubobjectPtr<class UCameraComponent> cameraComponent;


	//  Tower's projectile
	UPROPERTY(EditAnywhere, Category = Attack)
		TSubclassOf<class ATowerProjectile> projectileClass;

	//  Offset for projectile spawn
	UPROPERTY(EditAnywhere, Category = Attack)
		FVector projectileOffset;

	//	To spawn controller
	void Tick(float sec) override;

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser);

	//	Spawn the health bar
	void spawnHealthBar();

	void enter();
	void exit();
	bool isOccupied();
	void onFire(float currentPitch, float currentYaw, float currentRoll);
	float getHealth();
	void setHealth(float nhealth);
	float projectileOffsetLength;

private:
	bool occupied;
	
	void Death();

};