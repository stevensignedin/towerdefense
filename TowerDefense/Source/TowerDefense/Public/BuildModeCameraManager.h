// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TowerDefensePlayerController.h"
#include "Camera/PlayerCameraManager.h"
#include "BuildModeCameraManager.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ABuildModeCameraManager : public APlayerCameraManager
{
	GENERATED_UCLASS_BODY()

	void UpdateViewTarget(FTViewTarget& outVT, float deltaTime) override;

	FTViewTarget myFTViewTarget;

	bool viewTargetValid;

	ABuildModeCamera* myCamera;
};
