// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TowerDefensePlayerController.h"
#include "GameFramework/Actor.h"
#include "NexusTurret.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ANexusTurret : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = Structure)
	TSubobjectPtr<class UStaticMeshComponent> nexusMesh;

	UPROPERTY(EditAnywhere, Category = Structure)
	float Health;
	
	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser) override; 

	float getHealth();

};
