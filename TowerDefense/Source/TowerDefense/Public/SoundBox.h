// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SoundBox.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASoundBox : public AActor
{
	GENERATED_UCLASS_BODY()

	UAudioComponent* playSound(USoundCue* sound);
	UAudioComponent* playSound(USoundCue* sound, AActor* actorToPlayFrom);
	UAudioComponent* playSound(USoundCue* sound, AActor* actorToPlayFrom, bool stopWhenKilled);

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* backgroundMusic;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* impactFX;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* fireFX;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* zombieFX;
	
	//  Sound box's body
	UPROPERTY(VisibleInstanceOnly, Category = Structure)
		TSubobjectPtr<UBoxComponent> body;

	//	Play functions

	void playBackgroundMusic();
	void playImpactFX(AActor* hit);
	void playFireFX(AActor* hit);
	void playZombieFX();
};
