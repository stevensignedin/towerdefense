// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SoundBox.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "TowerProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATowerProjectile : public AActor
{
	GENERATED_UCLASS_BODY()

    //  Sphere collision component
    UPROPERTY(VisibleAnywhere, Category = Projectile)
    TSubobjectPtr<class USphereComponent> collisionComp;
    
    //  Projectile movement component
    UPROPERTY(VisibleAnywhere, Category = Movement)
    TSubobjectPtr<class UProjectileMovementComponent> projectileMovement;
    
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		UParticleSystem* HitEffect;


	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		float bulletDamage;

    //  Called when projectile hits something
    UFUNCTION()
	virtual void onActorBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
	void PostSpawnInitialize(FVector const& SpawnLocation, FRotator const& SpawnRotation, AActor* InOwner, APawn* InInstigator, bool bRemoteOwned, bool bNoFail, bool bDeferConstruction);
	
	void beginLifeTime();	

	UFUNCTION()
	void OnBounce(FHitResult const& HitResult);

	bool bShouldBounce = false; 
	static const int PROJECTILE_SPEED = 4000;
};
