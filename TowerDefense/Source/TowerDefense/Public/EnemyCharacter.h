// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "TowerDefenseCharacter.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"


/**
*
*/
UCLASS()
class TOWERDEFENSE_API AEnemyCharacter : public ACharacter
{

	GENERATED_UCLASS_BODY()

	// virtual float getHealth();

	//	Health bar
	AHealthBar* myHealthBar;
	UPROPERTY(EditAnywhere, Category = Health)
		TSubclassOf<class AHealthBar> healthBarClass;

	//	Array of different health bars
	TArray<UClass*> barTypes;
	float baseHealth;

	/***********************************************
		WHY IS TOWER CREEP HEALTH BAR SO BIG STILL????????????????????????
	***********************************************/

	bool Dead;
	bool Burnstatus;
	bool Freezestatus; 
	virtual void StartAttack();

	virtual void StopAttack();

	virtual void DealDamage();
	
	virtual int getMoneyValue();

	virtual void Slow();

	virtual void fixSpeed(); 

	virtual void Burn(); 

	virtual void burnDamage(); 

	virtual void Death();

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;

	void setHealth(float h);

	void spawnHealthBar();

	UPROPERTY(EditAnywhere, Category = Stats)
		float Health;

	UPROPERTY(EditAnywhere, Category = Stats)
		float AttackRange;

	UPROPERTY(EditAnywhere, Category = Stats)
		float Damage;

	UPROPERTY(EditAnywhere, Category = Stats)
		float AttackRate;

	UPROPERTY(EditDefaultsOnly, Category = Pawn)
		UParticleSystem* burn;

	UPROPERTY(EditDefaultsOnly, Category = Pawn)
		UParticleSystem* freeze;
};