// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EnemyCharacter.h"
#include "TowerProjectile.h"
#include "AIController.h"
#include "AICreepController.generated.h"

enum class AICreepControllerState : uint8
{
	Start,
	RunToTarget,
	AttackTarget,
	DestroyedTarget,
	NoTarget,
	Dead
};

UCLASS()
class TOWERDEFENSE_API AAICreepController : public AAIController
{
	GENERATED_UCLASS_BODY()

	float Range = 1000.f;

	void BeginPlay() override;

	void Tick(float Seconds) override;

	void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;

protected:

	AICreepControllerState CurrentState;
	AEnemyCharacter* myCreep;
	
};
