// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Engine/TargetPoint.h"
#include "TerrainGenerator.generated.h"

UCLASS()
class TOWERDEFENSE_API ATerrainGenerator : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleInstanceOnly, Category = Terrain)
	TSubobjectPtr<UBoxComponent> body;

	UPROPERTY(EditAnywhere, Category = Terrain)
	TArray<ATargetPoint*> SpawnPoints;  //The available spawn points

	UPROPERTY(EditAnywhere, Category = Terrain)
	TSubclassOf<AActor> CharacterToSpawn;  //Type of Character to spawn

	ATargetPoint* target;

	void Spawn();
	void BeginPlay() override;
	
	
};
