// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "HealthBar.generated.h"

/**
 *	
 */
class ABaseTower;
class AEnemyCharacter;
UCLASS()
class TOWERDEFENSE_API AHealthBar : public AActor
{
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(VisibleDefaultsOnly, Category = Structure)
	TSubobjectPtr<class UStaticMeshComponent> barMesh;

	//	Parent of this health bar
	ABaseTower* towerParent;
	AEnemyCharacter* charParent;
	
	//	Depth at 100% health
	float baseSize;

	//	Dimensions of the width and height
	float baseWidheight;

	//	Parent's max health
	float maxHealth;
	
	//	Constantly update health
	void Tick(float deltaSec) override;

	//	Increase size based on distance from player
	void distanceFactor(float distance, float healthRatio);
};
