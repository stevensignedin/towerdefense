// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseTower.h"
#include "IceTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AIceTower : public ABaseTower
{
	GENERATED_UCLASS_BODY()
	int cost = 150;
	float bulletDamage = 15.0f; 
};
