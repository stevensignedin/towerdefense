// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "Engine/TargetPoint.h"
#include "BaseTower.h"
#include "GameFramework/Actor.h"
#include "TowerManager.generated.h"
/**
*
*/
UCLASS()
class TOWERDEFENSE_API ATowerManager : public AActor
{
	GENERATED_UCLASS_BODY()

	/* VARIABLES */

	//  Manager's body
	UPROPERTY(VisibleInstanceOnly, Category = Tower)
	TSubobjectPtr<UBoxComponent> body;

	//  Array of all towers
	TArray<ABaseTower*> allTowers;
	//  Tower target points
	UPROPERTY(EditAnywhere, Category = Tower)
		TArray<ATargetPoint*> targetPoints;

	//  Tower type to spawn or something like that
	UPROPERTY(EditAnywhere, Category = Tower)
		TSubclassOf<class APawn> towerToSpawn;

	int32 towerID;

public:
	int32 current; 

	void updateTowerList();
	void BeginPlay() override;
	ABaseTower* Next(); //returns the next free tower, sets your current tower's occupied status to false, and sets the next tower occupied status to true
	ABaseTower* Previous();
	//	Spawn a tower at location
	void spawnTower(FVector location);

	//	Mark if a tower is unreachable
	bool markUnreachable(ABaseTower* tower);
};