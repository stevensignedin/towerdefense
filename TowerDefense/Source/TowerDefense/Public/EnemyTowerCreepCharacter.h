// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EnemyCharacter.h"
#include "BaseTower.h"
#include "EnemyTowerCreepCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AEnemyTowerCreepCharacter : public AEnemyCharacter
{
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UAnimMontage* AttackAnim;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UAnimMontage* DeathAnim;

	ABaseTower* attackingTower;

	void StartAttack();

	void StopAttack();

	void DealDamage();

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser);

	//void Slow() override;

	//void fixSpeed() override;

	void Burn() override;

	void burnDamage() override;

	void Death();
	
	float getHealth();

	void setAttackingTower(ABaseTower* tower);

	int getMoneyValue();

	void Destroy();
};
