// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EnemyCharacter.h"
#include "EnemyNexusCreepCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AEnemyNexusCreepCharacter : public AEnemyCharacter
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UAnimMontage* AttackAnim;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UAnimMontage* DeathAnim;

	void StartAttack();

	void StopAttack();

	void DealDamage();

	float getHealth();

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser);

	void Death();

	int getMoneyValue();

	void Destroy();

	void Burn() override;

	void burnDamage() override;
	
};
