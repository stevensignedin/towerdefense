// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/TargetPoint.h"
#include "GameFramework/Actor.h"
#include "ProjectileManager.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AProjectileManager : public AActor
{
	GENERATED_UCLASS_BODY()

	//  Tower type to spawn or something like that
	UPROPERTY(EditAnywhere, Category = Tower)
	TSubclassOf<class APawn> towerToSpawn;
	
};
