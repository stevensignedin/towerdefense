// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Engine/TargetPoint.h"
#include "SpawnManagerNexusCreep.generated.h"

/**
*
*/
UCLASS()
class TOWERDEFENSE_API ASpawnManagerNexusCreep : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category = Spawning)
	TArray<ATargetPoint*> WhereToSpawn;

	UPROPERTY(EditAnywhere, Category = Spawning)
		TSubclassOf<class ACharacter> CharacterToSpawn;

	UPROPERTY(EditAnywhere, Category = Spawning)
		float MinSpawnTime;

	UPROPERTY(EditAnywhere, Category = Spawning)
		float MaxSpawnTime;

	float SpawnDelay;
	float SpawnTime;

	bool bIsSpawningEnabled = true;

	void DoSpawn();

	void Tick(float DeltaSeconds);

	void EnableSpawning();
	void DisableSpawning();
	float GetRandomSpawnDelay();

};
