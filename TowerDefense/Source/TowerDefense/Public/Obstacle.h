// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Obstacle.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AObstacle : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleAnywhere, Category = Obstacle)
	TSubobjectPtr<UStaticMeshComponent> obstacleMesh;
	
};
