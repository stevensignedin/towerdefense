// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "TowerDefenseHUD.generated.h"

/**
 * 
 */
class ASpawnManager1;

UCLASS()
class TOWERDEFENSE_API ATowerDefenseHUD : public AHUD
{
	GENERATED_UCLASS_BODY()

	/*Font storage*/
	UPROPERTY()
	UFont* HUDFont;

	bool lostGame;
	bool wonGame;

	virtual void DrawHUD() override;

	void displayNextWave();
	
	void endGame();

	FVector2D ScreenDimensions;
	ASpawnManager1* spawnManager;
};
