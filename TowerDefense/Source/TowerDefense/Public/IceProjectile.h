// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TowerProjectile.h"
#include "IceProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AIceProjectile : public ATowerProjectile
{
	GENERATED_UCLASS_BODY()

	float bulletDamage = 5.0f;

	void onActorBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult); 	
};
