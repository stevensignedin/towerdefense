// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AICreepController.h"
#include "BaseTower.h"
#include "EnemyCharacter.h"
#include "EnemyTowerCreepCharacter.h"
#include "AIEnemyTowerCreepController.generated.h"

UCLASS()
class TOWERDEFENSE_API AAIEnemyTowerCreepController : public AAICreepController
{
	GENERATED_UCLASS_BODY()

	void BeginPlay() override;

	void Tick(float Seconds) override;

	void OnMoveCompleted(FAIRequestID RequestID,
		EPathFollowingResult::Type Result) override;

	ABaseTower* nextTarget();
};
