// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TowerDefensePlayerController.h"
#include "EnemyCharacter.h"
#include "BaseTower.h"
#include "AIController.h"
#include "AIBaseTowerController.generated.h"

enum class BaseTowerControllerState : uint8 {
	Occupied,
	NoTarget,
	AttackingTarget,
	Ghost	//	Don't do anything if build mode ghost tower
};

UCLASS()
class TOWERDEFENSE_API AAIBaseTowerController : public AAIController
{
	GENERATED_UCLASS_BODY()

	void BeginPlay() override;

	void Tick(float sec) override;

	void attack();
	void stopAttack();
	void startAttack();
	//	Tower's attack range
	float attackRange;

	//	Tower's attack rate
	float attackRate;

	FVector getVectorToAimIn(APawn* enemy);
	AEnemyCharacter* getClosestEnemy();
	void setCurrentState(BaseTowerControllerState newState);

private:

	BaseTowerControllerState currentState;

	ABaseTower* myTower;

	AEnemyCharacter* creepTarget;
	
};
