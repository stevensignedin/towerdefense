// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Camera/PlayerCameraManager.h"
#include "TowerPlayerCameraManager.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATowerPlayerCameraManager : public APlayerCameraManager
{
	GENERATED_UCLASS_BODY()
    
    float panSpeed;

    void UpdateViewTarget(FTViewTarget& outVT, float deltaTime) override;

    bool viewTargetValid;

	bool towerChanged; 

	void toggleTowerChanged(); 
};
