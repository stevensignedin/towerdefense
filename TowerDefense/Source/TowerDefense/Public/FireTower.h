// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseTower.h"
#include "FireTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AFireTower : public ABaseTower
{
	GENERATED_UCLASS_BODY()
	float bulletDamage = 15.0f;
	int cost = 150;
};
