 // Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "SoundBox.h"
#include "BaseTower.h"
#include "IceTower.h"
#include "BuildModeCamera.h"
#include "TowerPlayerCameraManager.h"
#include "TowerDefenseCharacter.h"
#include "TowerManager.h"
#include "GameFramework/InputSettings.h"
#include "GameFramework/PlayerController.h"
#include "TowerDefensePlayerController.generated.h"

UCLASS()
class ATowerDefensePlayerController : public APlayerController
{
	GENERATED_UCLASS_BODY()

	//	The tower that will be built in build mode
	TSubclassOf<class ABaseTower> towerToBuild;

	//Array of different towers
	TArray<UClass*> towerTypes;

	//	If my tower is occupied
	bool towerOccupied;

	//	If in build mode or  not
	bool inBuildMode;

	//	If the player is looking left or right
	bool lookingLeft, lookingRight;

	//	If player is panning build camera
	bool panningLeft, panningRight, panningUp, panningDown;

	//	If player is zooming in/out
	bool zoomingOut, zoomingIn;

	bool start; // for the start of the game
	//dont need this janky stuff no more
	// the hud handles this (which is even
	// jankier)
	// bool finished; // for the end of the game
	void makeFinished();

	//base tower selected
	void baseTowerType();

	//ice tower selected
	void iceTowerType(); 

	//fire tower selected
	void fireTowerType(); 

	//	The sound box
	ASoundBox* mySoundBox;

	//counter for towertype switching purposes
	int32 counter; 
	static void printMessage(FString string);

protected:

	UPROPERTY(Transient)
	UAudioComponent* backgroundAC;

	
	// finishing the game
	
	ATowerDefenseCharacter* myPlayer;

	//	Ghost tower projected when in build mode
	ABaseTower* myGhostTower;

	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;
	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	//  Occupy a tower
	void fireMode();

	//	Go to build mode
	void buildMode();

	//	Place tower at mouse location
	void placeTower();

	// Panning the tower camera
	void lookLeft();
	void stopLeft();
	void lookRight();
	void stopRight();

	//	Panning the build camera
	void panLeft();
	void panRight();
	void panUp();
	void panDown();
	void stopPanLeft();
	void stopPanRight();
	void stopPanUp();
	void stopPanDown();

	//	Zooming the build camera
	void zoomOut();
	void zoomIn();

	//	Set keys for firing mode
	void setFireMappings();

	//	Set keys for build mode
	void setBuildMappings();

	//	For refreshing key mappings
	UInputSettings* removeRelevantMappings(UInputSettings* uis);

	//  Fire a single shot
	void playerFire();

	//	Allows for auto fire
	void autoFire();
	void stopAutoFire();
	float autoFireRate;

	// to move to next tower
	void nextTower();
	void previousTower();
	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();
	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);
	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
};