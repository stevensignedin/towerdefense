// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TowerDefenseHUD.h"
#include "GameFramework/Actor.h"
#include "Engine/TargetPoint.h"
#include "EnemyCharacter.h"
#include "SpawnManager1.generated.h"


/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASpawnManager1 : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category = Spawning)
	TArray<ATargetPoint*> WhereToSpawn;

	UPROPERTY(EditAnywhere, Category = Spawning)
	TSubclassOf<class AEnemyNexusCreepCharacter> EnemyNexusCreep;

	UPROPERTY(EditAnywhere, Category = Spawning)
	TSubclassOf<class AEnemyTowerCreepCharacter> EnemyTowerCreep;

	UPROPERTY(EditAnywhere, Category = Spawning)
		float MinSpawnTime;

	UPROPERTY(EditAnywhere, Category = Spawning)
		float MaxSpawnTime;

	//	Number of enemies per wave
	int32 enemiesPerWave[5];

	//	Seconds between end of last spawned this wave and first spawned next wave
	int32 timePerWave[5];

	//	Health multiplier for spawn enemies
	float healthMultiplier[5];

	//	Max spawn times per wave
	float maxSpawnTimes[5];

	int32 maxEnemies;
	int32 enemyCount; 
	int32 wave; 

	bool start;
	float SpawnDelay;
	float clock;

	bool displayNextWave; 
	bool enableSpawning;

	//	The sound box
	ASoundBox* mySoundBox;

	void DoSpawn();

	void Tick(float DeltaSeconds);
	int32 getWaveNumber();
	int32 getNumEnemies(); 

	void EnableSpawning();
	void DisableSpawning();
	float GetRandomSpawnDelay();
	
};
