// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include <time.h>
#include <stdlib.h>
#include "BaseTower.h"
#include "TowerManager.h"
#include "TowerDefenseCharacter.generated.h"
UCLASS(Blueprintable)
class ATowerDefenseCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

	/*** VARIABLES ***/
	/** Player camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	TSubobjectPtr<class UCameraComponent> cameraComponent;
	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		TSubobjectPtr<class USpringArmComponent> CameraBoom;
	//  Tower the player is occupying
	ABaseTower* myTower;


	int32 currency;

	/*** FUNCTIONS ***/

	//	Enter the first tower in memory
	void enterTower();

	//	Exit the tower (go into build mode)
	void exitTower();

	//	Enter the given tower
	void enterTower(ABaseTower* tower);

	void nextTower();
	void previousTower(); 

	int getCurrency();
	void setCurrency(int ncurrency);
	void addCurrency(int toAdd);
};