// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BuildModeCamera.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ABuildModeCamera : public AActor
{
	GENERATED_UCLASS_BODY()

	//	The actual camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	TSubobjectPtr<class UCameraComponent> cameraComponent;

	//	Object's body
	UPROPERTY(VisibleInstanceOnly, Category = Camera)
	TSubobjectPtr<UBoxComponent> body;

	UPROPERTY(EditAnywhere, Category = Control)
	float panSpeed;

	UPROPERTY(EditAnywhere, Category = Control)
	float zoomSpeed;
};
