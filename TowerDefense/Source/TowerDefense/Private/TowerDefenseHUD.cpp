// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "TowerDefenseHUD.h"
#include "NexusTurret.h"
#include "EnemyCharacter.h"
#include "EnemyNexusCreepCharacter.h"
#include "BaseTower.h"
#include "SpawnManager1.h"
#include "TowerDefensePlayerController.h"
#include "EnemyTowerCreepCharacter.h"
#include "Engine/Canvas.h"
#include "Engine/Font.h"


ATowerDefenseHUD::ATowerDefenseHUD(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	static ConstructorHelpers::FObjectFinder<UFont> HUDFontOb(TEXT("/Engine/EngineFonts/RobotoDistanceField"));

	HUDFont = HUDFontOb.Object;
	lostGame = false;
	wonGame = false;
	ScreenDimensions = FVector2D(1, 1);
}

void ATowerDefenseHUD::endGame()
{
	FVector2D GameOverSize;
	if(Canvas)
	{
		if (lostGame) {
			GetTextSize(TEXT("GAME OVER"), GameOverSize.X, GameOverSize.Y, HUDFont);
			DrawText(TEXT("GAME OVER"), FColor::Red, (ScreenDimensions.X - GameOverSize.X) / 2.0f, (ScreenDimensions.Y - GameOverSize.Y) / 2.0f, HUDFont);
		}
		else if (wonGame) {
			GetTextSize(TEXT("YOU WIN!"), GameOverSize.X, GameOverSize.Y, HUDFont);
			DrawText(TEXT("YOU WIN!"), FColor::Green, (ScreenDimensions.X - GameOverSize.X) / 2.0f, (ScreenDimensions.Y - GameOverSize.Y) / 2.0f, HUDFont);
		}

		UGameplayStatics::SetGamePaused(GetWorld(), true);
	}
}

void ATowerDefenseHUD::DrawHUD()
{
	if (!Canvas)
		return;

	if (!spawnManager) {
		TArray<AActor*> actors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnManager1::StaticClass(), actors);
		if (actors.Num() > 0)
			spawnManager = (ASpawnManager1*)actors[0];

		return;
	}

	//	Get Screen Dimmensions 
	ScreenDimensions = FVector2D(Canvas->SizeX, Canvas->SizeY);

	Super::DrawHUD();

	if (lostGame || wonGame)
		endGame();
	else
	{
		TArray<AActor*> output;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ANexusTurret::StaticClass(), output);
		if (output.Num() > 0)
		{
			ANexusTurret* nexus = (ANexusTurret*)output[0];
			if (nexus)
			{
				FVector test = nexus->GetActorLocation();
				test = AHUD::Project(test);
				FString NexusHealth = FString::SanitizeFloat(nexus->getHealth());
				float nexusHealth = nexus->getHealth();
				//DrawText(NexusHealth, FColor::Red, (Canvas->SizeX) / 2, 10, HUDFont);
				FColor col = FColor::Green;
				if (nexusHealth < 750)
					col = FColor::Yellow;
				if (nexusHealth < 250)
					col = FColor::Red;

				DrawText(FString::Printf(TEXT("NEXUS HEALTH:")), FColor::Red, (10), (10), HUDFont);
				FCanvasTileItem RectItem(
					FVector2D((ScreenDimensions.X / 5), 10),
					FVector2D(nexusHealth / 1.25, 20),
					col
					);
				FColor backColor = FColor::White;
				FCanvasTileItem BackGroundRectItem(
					FVector2D((ScreenDimensions.X / 5), 10),
					FVector2D(1000 / 1.25, 20),
					backColor
					);
				Canvas->DrawItem(BackGroundRectItem);
				Canvas->DrawItem(RectItem);

				//	Display next wave if applicable
				if (spawnManager->displayNextWave)
				{
					DrawText(FString::Printf(TEXT("WAVE NUMBER: %d"), spawnManager->getWaveNumber()), FColor::White, ScreenDimensions.X / 2 - 20, ScreenDimensions.Y / 4, HUDFont);
					DrawText(FString::Printf(TEXT("%d ENEMY CREEPS"), spawnManager->getNumEnemies()), FColor::White, ScreenDimensions.X / 2 - 20, ScreenDimensions.Y / 4 + 20, HUDFont);
				}

				// outputting health for your tower, only if you're not in build mode. 
				TArray<AActor*> list; 
				UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerDefensePlayerController::StaticClass(), list);
				if (((ATowerDefensePlayerController*)list[0])->towerOccupied)
				{
					for (TActorIterator<ABaseTower> ActorItr(GetWorld()); ActorItr; ++ActorItr)
					{
						// int subtractThis;
						FVector test = ActorItr->GetActorLocation();
						test = AHUD::Project(test);
						ABaseTower*  tower = reinterpret_cast<ABaseTower*>(*ActorItr);
						float health = tower->getHealth();
						if (tower)
						{

							if (tower->isOccupied())
							{
								DrawText(FString::Printf(TEXT("MY TOWER HEALTH:")), FColor::Red, (10), (50), HUDFont);
								FCanvasTileItem RectItem(
									FVector2D(ScreenDimensions.X / 5 +25, 50),
									FVector2D(health / 2, 20),
									col
									);
								FColor backColor = FColor::White;
								FCanvasTileItem BackGroundRectItem(
									FVector2D((ScreenDimensions.X /5 + 25), 50),
									FVector2D(health / 2, 20),
									backColor
									);
								Canvas->DrawItem(BackGroundRectItem);
								Canvas->DrawItem(RectItem);
							}
						}
					}
				}
				

				ATowerDefenseCharacter* MyCharacter = Cast<ATowerDefenseCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
				if (MyCharacter)
				{
					int currency = MyCharacter->getCurrency();
					DrawText(FString::Printf(TEXT("Gold: %d"), currency), FColor::Yellow, (10), (75), HUDFont);
				}
			}
		}



		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerDefensePlayerController::StaticClass(), output);

		// there's only one
		if (output.Num() > 0)
		{
			ATowerDefensePlayerController * theController = Cast<ATowerDefensePlayerController>(output[0]);

		}
	}
}