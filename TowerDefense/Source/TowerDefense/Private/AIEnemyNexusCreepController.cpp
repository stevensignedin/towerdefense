// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "AIEnemyNexusCreepController.h"
#include "EnemyCharacter.h"
#include "TowerProjectile.h"
#include "NexusTurret.h"

AAIEnemyNexusCreepController::AAIEnemyNexusCreepController(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{

}

//	Immediately go for the nexus
void AAIEnemyNexusCreepController::BeginPlay()
{
	Super::BeginPlay();
	CurrentState = AICreepControllerState::Start;
}

void AAIEnemyNexusCreepController::Tick(float Seconds)
{
	Super::Tick(Seconds);

	if (!myCreep) {
		myCreep = (AEnemyNexusCreepCharacter*) this->GetPawn();
		return;
	}

	else if (CurrentState == AICreepControllerState::Dead)
		return;

	else if (((AEnemyNexusCreepCharacter*)myCreep)->getHealth() <= 0)
	{
		this->StopMovement();
		((AEnemyNexusCreepCharacter*)myCreep)->Death();

		CurrentState = AICreepControllerState::Dead;
	}

	else if (CurrentState == AICreepControllerState::AttackTarget) {
		//	Check if nexus is destroyed
		TArray<AActor*> output;

		ANexusTurret* nexus = getNexus();

		if (!nexus) {
			//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("DeadNexus")));
			myCreep->StopAttack();
			CurrentState = AICreepControllerState::DestroyedTarget;
		}
	}
	else if (CurrentState == AICreepControllerState::Start) {
		TArray<AActor*> output;

		ANexusTurret* nexus = getNexus();
		if (nexus) {
			CurrentState = AICreepControllerState::RunToTarget;
			//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("MovingToNexus")));
			MoveToActor(nexus);
		}
	}
}

//	Attack once reached nexus
void AAIEnemyNexusCreepController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	if (myCreep && Result == EPathFollowingResult::Success) {
		CurrentState = AICreepControllerState::AttackTarget;
		//if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Reached MoveCompleted")));
		if (myCreep)
			myCreep->StartAttack();
	}
}

//	Find the game nexus
ANexusTurret* AAIEnemyNexusCreepController::getNexus() {
	TArray<AActor*> output;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ANexusTurret::StaticClass(), output);

	int32 x = output.Num();

	if (x > 0) {
		ANexusTurret* nexus = (ANexusTurret*)output[0];
		//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("NexusExists")));
		if (nexus)
			return nexus;
		else
			return NULL;
	}
	else
		return NULL;
}