// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "SpawnManagerNexusCreep.h"
#include "EnemyNexusCreepCharacter.h"


ASpawnManagerNexusCreep::ASpawnManagerNexusCreep(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{

	// Set the StaticMeshComponent as the root component.

	// Set the spawn delay range and get the first SpawnDelay.
	MinSpawnTime = 3.5f;
	MaxSpawnTime = 5.5f;
	SpawnDelay = GetRandomSpawnDelay();

	PrimaryActorTick.bCanEverTick = true;
}

void ASpawnManagerNexusCreep::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (!bIsSpawningEnabled)
	{
		return;
	}

	// Always add delta time to our spawn time.
	SpawnTime += DeltaSeconds;

	bool bShouldSpawn = (SpawnTime > SpawnDelay);

	if (bShouldSpawn)
	{
		DoSpawn();

		// Deduct spawn delay from accumulated time value.
		SpawnTime -= SpawnDelay;

		SpawnDelay = GetRandomSpawnDelay();
	}
}

void ASpawnManagerNexusCreep::EnableSpawning()
{
	bIsSpawningEnabled = true;
}
void ASpawnManagerNexusCreep::DisableSpawning()
{
	bIsSpawningEnabled = false;
}


float ASpawnManagerNexusCreep::GetRandomSpawnDelay()
{
	// Get a random float that falls within the spawn delay range.
	return FMath::FRandRange(MinSpawnTime, MaxSpawnTime);
}

void ASpawnManagerNexusCreep::DoSpawn()
{
	if (CharacterToSpawn != NULL)
	{
		// Check for a valid World:
		UWorld* const World = GetWorld();
		if (World)
		{
			// Set the spawn parameters.
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;

			//get a random location in the TArray, only 4 values in it
			const int32 randVal = FMath::RandRange(0, WhereToSpawn.GetAllocatedSize() / WhereToSpawn.GetTypeSize() - 1);
			// Spawn the pickup.
			FRotator SpawnRotation;
			SpawnRotation.Yaw = 0.0f;
			SpawnRotation.Pitch = 0.0f;
			SpawnRotation.Roll = 0.0f;
			ATargetPoint * temp = WhereToSpawn[randVal];
			FVector Location = temp->GetActorLocation();

			// now spawn it
			AEnemyNexusCreepCharacter* SpawnedEnemy = World->SpawnActor<AEnemyNexusCreepCharacter>(CharacterToSpawn, Location, SpawnRotation, SpawnParams);
			if (SpawnedEnemy)
			{
				SpawnedEnemy->SpawnDefaultController();
			}
		}
	}
}