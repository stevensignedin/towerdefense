// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "TowerDefensePlayerController.h"
#include "BaseTower.h"
#include "HealthBar.h"
#include "EnemyCharacter.h"

AHealthBar::AHealthBar(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	barMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Bar Mesh"));
	barMesh->SetSimulatePhysics(false);
	RootComponent = barMesh;

	baseSize = 2.5f;
	baseWidheight = 0.3f;
	RootComponent->SetRelativeScale3D(FVector(baseWidheight, baseSize, baseWidheight));

	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

void AHealthBar::Tick(float deltaSec) {
	Super::Tick(deltaSec);
	
	if (towerParent || charParent) {
		//	Update so it's always facing the player
		APlayerController* controller = GetWorld()->GetFirstPlayerController();
		if (controller) {
			ATowerDefensePlayerController* con = (ATowerDefensePlayerController*)controller;
			AActor* actor = con->GetViewTarget();

			FVector barLoc = this->GetActorLocation();
			FVector playerLoc = actor->GetActorLocation();

			//	Direction to face
			FVector direction = playerLoc - barLoc;

			this->SetActorRotation(direction.Rotation());

			//	Update the health
			if (charParent) {
				float currHealth = charParent->Health;
				float ratio = currHealth / maxHealth;
				distanceFactor(FVector::Dist(barLoc, playerLoc), ratio);

				//	Also update location
				if (charParent && !charParent->IsPendingKill() && this)
				{
					FVector loc = charParent->GetActorLocation();
					loc.Set(loc.X, loc.Y, loc.Z + 300);

					if (this)
						this->SetActorLocation(loc);
				}
			}
			else if (towerParent) {
				float currHealth = towerParent->getHealth();
				float ratio = currHealth / maxHealth;
				distanceFactor(FVector::Dist(barLoc, playerLoc), ratio);
			}
		}
	}
}

void AHealthBar::distanceFactor(float distance, float healthRatio) {
	float increase = 0.1*(distance / 100);

	float currSize = (baseSize + increase) * healthRatio;
	float currWidheight = baseWidheight + (increase / 6.0f);

	RootComponent->SetRelativeScale3D(FVector(currWidheight, currSize, currWidheight));
}