// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "IceProjectile.h"
#include "EnemyCharacter.h"


AIceProjectile::AIceProjectile(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	bulletDamage = 10; 
}

void AIceProjectile::onActorBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(this, HitEffect, this->GetActorLocation());
	AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(OtherActor);
	if (Enemy)
	{
		Enemy->Slow(); 
		Enemy->TakeDamage(bulletDamage, FDamageEvent(), GetInstigatorController(), this);

		//	Play impact sound
		TArray<AActor*> actors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASoundBox::StaticClass(), actors);
		if (actors.Num() > 0) {
			((ASoundBox*)actors[0])->playImpactFX(this);
		}
	}
	SetActorHiddenInGame(true); 
	projectileMovement->StopMovementImmediately();
	SetLifeSpan(1.0f);
}