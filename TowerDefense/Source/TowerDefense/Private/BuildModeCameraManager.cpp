// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "BuildModeCameraManager.h"


ABuildModeCameraManager::ABuildModeCameraManager(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	viewTargetValid = false;
}

void ABuildModeCameraManager::UpdateViewTarget(FTViewTarget &outVT, float deltaTime) {
	if (!myCamera) {
		AActor* actor = this->GetViewTarget();
		myCamera = Cast<ABuildModeCamera>(actor);
		return;
	}

	//	Let player pan camera around
	ATowerDefensePlayerController* control = (ATowerDefensePlayerController*) this->GetOwningPlayerController();
	if (control->panningDown || control->panningLeft || control->panningRight ||
			control->panningUp || control->zoomingIn || control->zoomingOut) {

		FVector location = outVT.POV.Location;
		if (control->panningDown)
			location.Set(location.X - myCamera->panSpeed*deltaTime, location.Y, location.Z);

		if (control->panningUp)
			location.Set(location.X + myCamera->panSpeed*deltaTime, location.Y, location.Z);

		if (control->panningLeft)
			location.Set(location.X, location.Y - myCamera->panSpeed*deltaTime, location.Z);

		if (control->panningRight)
			location.Set(location.X, location.Y + myCamera->panSpeed*deltaTime, location.Z);
		if (control->zoomingIn) {
			control->zoomingIn = false;
			location.Set(location.X, location.Y, location.Z - myCamera->zoomSpeed);
		}
		if (control->zoomingOut) {
			control->zoomingOut = false;
			location.Set(location.X, location.Y, location.Z + myCamera->zoomSpeed);
		}

		outVT.POV.Location = location;

		// myCamera->setLocation(location);
		this->SetActorLocationAndRotation(outVT.POV.Location, outVT.POV.Rotation, true);
		myCamera->SetActorLocationAndRotation(outVT.POV.Location, outVT.POV.Rotation, true);
		myFTViewTarget = outVT;
		viewTargetValid = true;
	}
	else {
		if (viewTargetValid)
			Super::UpdateViewTarget(myFTViewTarget, deltaTime);
		else
			Super::UpdateViewTarget(outVT, deltaTime);
	}
}