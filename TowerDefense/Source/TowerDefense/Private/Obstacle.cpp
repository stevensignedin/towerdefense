// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "Obstacle.h"

AObstacle::AObstacle(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	obstacleMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Obstacle Mesh"));
	obstacleMesh->SetSimulatePhysics(true);
	RootComponent = obstacleMesh;
}