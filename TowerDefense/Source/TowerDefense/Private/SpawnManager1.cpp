// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "SpawnManager1.h"
#include "EnemyCharacter.h"
#include "EnemyNexusCreepCharacter.h"
#include "EnemyTowerCreepCharacter.h"
#include <time.h>
#include "string.h"
#include <iostream>
#include <typeinfo>


ASpawnManager1::ASpawnManager1(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{
	int i = 0;
	enemiesPerWave[i++] = 10; enemiesPerWave[i++] = 25; enemiesPerWave[i++] = 40;
	enemiesPerWave[i++] = 55; enemiesPerWave[i++] = 70;
	i = 0;
	timePerWave[i++] = 10; timePerWave[i++] = 10; timePerWave[i++] = 15;
	timePerWave[i++] = 20; timePerWave[i++] = 25;
	i = 0;
	healthMultiplier[i++] = 1.0f; healthMultiplier[i++] = 1.5f; healthMultiplier[i++] = 2.0f;
	healthMultiplier[i++] = 2.5f; healthMultiplier[i++] = 3.0f;
	i = 0;
	maxSpawnTimes[i++] = 6.0f; maxSpawnTimes[i++] = 5.0f; maxSpawnTimes[i++] = 4.0f;
	maxSpawnTimes[i++] = 3.0f; maxSpawnTimes[i++] = 2.0f;

	clock = 0;
	wave = 1;
	maxEnemies = enemiesPerWave[0]; 
	enemyCount = 0; 
	displayNextWave = false;
	// Set the StaticMeshComponent as the root component.
	
	// Set the spawn delay range and get the first SpawnDelay.
	MinSpawnTime = 1.0f;
	MaxSpawnTime = maxSpawnTimes[0];
	SpawnDelay = GetRandomSpawnDelay();
	enableSpawning = true;

	PrimaryActorTick.bCanEverTick = true;
	srand(time(NULL));
	start = true;
}

void ASpawnManager1::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (!mySoundBox) {
		TArray<AActor*> output;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASoundBox::StaticClass(), output);
		if (output.Num() > 0)
			mySoundBox = (ASoundBox*)output[0];

		return;
	}

	if (start)
	{
		enableSpawning = false;
		clock += DeltaSeconds;
		if (clock > 10.0f)
		{
			this->EnableSpawning();
			start = false;
			clock = 0;
		}
	}
	//	If still releasing the current wave
	if (enemyCount < maxEnemies && enableSpawning) {
		clock += DeltaSeconds;

		if (clock > SpawnDelay) {
			displayNextWave = false;
			//	Release a creep
			DoSpawn();

			// Deduct spawn delay and get a new one
			clock -= SpawnDelay;

			SpawnDelay = GetRandomSpawnDelay();
		}
	}
	else if (enableSpawning) {
		//	Advance a wave
		if (++wave > 5) {
			//	Make sure all creeps are dead
			TArray<AActor*> actors;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyCharacter::StaticClass(), actors);
			if (actors.Num() == 0) {
				//	You win!!
				TArray<AActor*> actors;
				UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerDefenseHUD::StaticClass(), actors);
				if (actors.Num() > 0) {
					ATowerDefenseHUD* myHUD = (ATowerDefenseHUD*)actors[0];
					myHUD->wonGame = true;
				}
			}

			return;
		}

		enableSpawning = false;
		enemyCount = 0;
		maxEnemies = enemiesPerWave[wave - 1];
		MaxSpawnTime = maxSpawnTimes[wave - 1];

		//	Wait for the intermission time
		GetWorldTimerManager().SetTimer(this, &ASpawnManager1::EnableSpawning, timePerWave[wave - 1], false);
	}
}

void ASpawnManager1::EnableSpawning()
{
	enableSpawning = true;
	displayNextWave = true;
	
	//	Also play zombie sound
	mySoundBox->playZombieFX();
}
void ASpawnManager1::DisableSpawning()
{
	enableSpawning = false;
}

int32 ASpawnManager1::getWaveNumber()
{
	return wave; 
}
int32 ASpawnManager1::getNumEnemies()
{
	return maxEnemies; 
}
float ASpawnManager1::GetRandomSpawnDelay()
{
	// Get a random float that falls within the spawn delay range.
	return FMath::FRandRange(MinSpawnTime, MaxSpawnTime);
}

void ASpawnManager1::DoSpawn()
{
	enemyCount++;

	if (EnemyNexusCreep && EnemyTowerCreep)
	{
		// Check for a valid World:
		UWorld* const World = GetWorld();
		if (World)
		{
			// Set the spawn parameters
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;

			//get a random location in the TArray, only 4 values in it
			int random = rand() % WhereToSpawn.Num();
			// const int32 randVal = FMath::RandRange(0, WhereToSpawn.GetAllocatedSize() / WhereToSpawn.GetTypeSize() - 1);
			FRotator SpawnRotation;
			SpawnRotation.Yaw = 0.0f;
			SpawnRotation.Pitch = 0.0f;
			SpawnRotation.Roll = 0.0f;
			ATargetPoint * temp = WhereToSpawn[random];
			FVector Location = temp->GetActorLocation();

			// now spawn it
			random = rand() % 2;
			
			if (random < 1)
			{
				AEnemyNexusCreepCharacter* const SpawnedEnemy = World->SpawnActor<AEnemyNexusCreepCharacter>(EnemyNexusCreep, Location, SpawnRotation, SpawnParams);

				// give it a controller
				if (SpawnedEnemy)
				{
					SpawnedEnemy->setHealth(SpawnedEnemy->baseHealth*healthMultiplier[wave-1]);
					SpawnedEnemy->baseHealth = SpawnedEnemy->getHealth();
					SpawnedEnemy->SpawnDefaultController();
					SpawnedEnemy->spawnHealthBar();
					
				}
			}
			else // random == 1
			{
				AEnemyTowerCreepCharacter* const SpawnedEnemy = World->SpawnActor<AEnemyTowerCreepCharacter>(EnemyTowerCreep, Location, SpawnRotation, SpawnParams);

				// give it a controller
				if (SpawnedEnemy)
				{
					SpawnedEnemy->setHealth(SpawnedEnemy->baseHealth*healthMultiplier[wave - 1]);
					SpawnedEnemy->baseHealth = SpawnedEnemy->getHealth();
					SpawnedEnemy->SpawnDefaultController();
					SpawnedEnemy->spawnHealthBar();
				}
			}
		}
	}
}