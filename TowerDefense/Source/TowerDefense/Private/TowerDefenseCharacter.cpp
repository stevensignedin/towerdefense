// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#include "TowerDefense.h"
#include "TowerDefensePlayerController.h"
#include "TowerDefenseCharacter.h"
ATowerDefenseCharacter::ATowerDefenseCharacter(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{

	// Set size for player capsule
	CapsuleComponent->InitCapsuleSize(42.f, 96.0f);
	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	// Configure character movement
	CharacterMovement->bOrientRotationToMovement = true; // Rotate character to moving direction
	CharacterMovement->RotationRate = FRotator(0.f, 640.f, 0.f);
	CharacterMovement->bConstrainToPlane = true;
	CharacterMovement->bSnapToPlaneAtStart = true;
	// Create a camera boom...
	CameraBoom = PCIP.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera
	cameraComponent = PCIP.CreateDefaultSubobject<UCameraComponent>(this, TEXT("PlayerCamera"));
	cameraComponent->AttachTo(CameraBoom, USpringArmComponent::SocketName);
	cameraComponent->bUsePawnControlRotation = false;

	currency = 400; 
}

void ATowerDefenseCharacter::enterTower() {
	TArray<AActor*> list;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABaseTower::StaticClass(), list);

	if (list.Num() > 0) {
		myTower = (ABaseTower*)list[0];
		myTower->enter();
	}
}

void ATowerDefenseCharacter::exitTower() {
	myTower->exit();
	myTower = nullptr;
}

void ATowerDefenseCharacter::enterTower(ABaseTower* tower) {
	TArray<AActor*> list;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABaseTower::StaticClass(), list);

	if (list.Num() > 0) {
		for (int i = 0; i < list.Num(); i++) {
			if (tower == list[i]) {
				myTower = (ABaseTower*)list[i];
				myTower->enter();
			}
		}
	}
}

void ATowerDefenseCharacter::nextTower()
{
	TArray<AActor*> list;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), list);

	if (list[0] != nullptr)
	{
		myTower = ((ATowerManager*)list[0])->Next();
		myTower->enter(); 
	}
}
void ATowerDefenseCharacter::previousTower()
{
	TArray<AActor*> list;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), list);
	if(list[0] != nullptr)
	{
		myTower = ((ATowerManager*) list[0])->Previous();
		myTower->enter();
	}
}

int ATowerDefenseCharacter::getCurrency()
{
	return currency;
}

void ATowerDefenseCharacter::setCurrency(int ncurrency)
{
	currency = ncurrency;
}

void ATowerDefenseCharacter::addCurrency(int toAdd) {
	currency += toAdd;
}