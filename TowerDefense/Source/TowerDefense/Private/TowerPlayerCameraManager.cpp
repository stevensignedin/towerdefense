// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "TowerPlayerCameraManager.h"
#include "TowerDefensePlayerController.h"

ATowerPlayerCameraManager::ATowerPlayerCameraManager(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	panSpeed = 95.0f;
    viewTargetValid = false;
	towerChanged = false; 
}

void ATowerPlayerCameraManager::UpdateViewTarget(FTViewTarget &outVT, float deltaTime) {

    ATowerDefensePlayerController* control = (ATowerDefensePlayerController*) this->GetOwningPlayerController();
	if (control) {
		ABaseTower* theTower = ((ATowerDefenseCharacter*)control->GetPawn())->myTower;
		if (theTower) {
			FVector offset = theTower->projectileOffset;

			if (towerChanged || !viewTargetValid)
				Super::UpdateViewTarget(outVT, deltaTime);

			if (control->towerOccupied && (!viewTargetValid || towerChanged || control->lookingLeft ||
				control->lookingRight || control->panningDown || control->panningUp)) {
				FRotator rote = theTower->myVT.POV.Rotation;
				if (control->lookingLeft)
				{
					rote.Add(0, (-1)*(panSpeed)*deltaTime, 0);
				}
				else if (control->lookingRight)
				{
					rote.Add(0, (panSpeed)*deltaTime, 0);
				}
				if (control->panningUp)
				{
					rote.Add(panSpeed*deltaTime, 0, 0);

					//	Can't pan past 70 degrees up
					int32 pitch = rote.Pitch;
					if (pitch > 70)
						rote.Add(panSpeed*deltaTime*(-1), 0, 0);
				}
				else if (control->panningDown)
				{
					rote.Add(panSpeed*deltaTime*-1, 0, 0);

					//	Can't pan past 70 degrees down
					int32 pitch = rote.Pitch;
					if (pitch < -70)
						rote.Add(panSpeed*deltaTime, 0, 0);
				}

				//	Idk why but i have to do it here to preserve camera rotation
				if (towerChanged) {
					toggleTowerChanged();
					rote.Add(0, 1, 0);
				}

				//	Same with this one (this is for when you first enter a tower from build mode)
				if (!viewTargetValid) {
					rote.Add(0, 1, 0);
					viewTargetValid = true;
				}

				outVT.POV.Rotation = rote;
				theTower->myVT.POV.Rotation = rote;

				// the distance from the projectile (hopefully this is enuf so that it rotates all the way around
				this->SetActorLocationAndRotation(outVT.POV.Location + offset, outVT.POV.Rotation, true);
			}
		}
	}
}

void ATowerPlayerCameraManager::toggleTowerChanged()
{
	if(towerChanged)
	{
		towerChanged = false;
	}
	else
	{
		towerChanged = true;
	}
}

