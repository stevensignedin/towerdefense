// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "EnemyTowerCreepCharacter.h"
#include "NexusTurret.h"
#include "AIEnemyTowerCreepController.h"


AEnemyTowerCreepCharacter::AEnemyTowerCreepCharacter(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{
	AIControllerClass = AAIEnemyTowerCreepController::StaticClass();

	Health = baseHealth = 100.0f;
	AttackRange = 200.0f;
	Damage = 10.0f;
	AttackRate = 1.0f;
	Dead = false; 
	Burnstatus = false; 
}
void AEnemyTowerCreepCharacter::DealDamage()
{
	// if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Calling take damage")));
	if(attackingTower)
	{
		// if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Calling take damage")));
		attackingTower->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
	}

}
void AEnemyTowerCreepCharacter::StartAttack()
{
	// PlayAnimMontage(AttackAnim);
	
	// if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Starting Attack Animation")));
	//AActor* PlayerCharacter = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (this)
		GetWorldTimerManager().SetTimer(this, &AEnemyTowerCreepCharacter::DealDamage, PlayAnimMontage(AttackAnim), true);
	// GetWorldTimerManager().SetTimer(this, &AEnemyTowerCreepCharacter::DealDamage, AttackRate, true);
}

void AEnemyTowerCreepCharacter::StopAttack()
{
	StopAnimMontage(AttackAnim);
	// if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Stopping Attack Animation")));
	GetWorld()->GetTimerManager().ClearTimer(this, &AEnemyTowerCreepCharacter::DealDamage);
}

float AEnemyTowerCreepCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void AEnemyTowerCreepCharacter::Death()
{
	Super::Death();
	// standard death stuff
	StopAttack();
	Dead = true; 
	GetWorldTimerManager().SetTimer(this, &AEnemyTowerCreepCharacter::Destroy, PlayAnimMontage(DeathAnim) - 0.4, false);
}

void AEnemyTowerCreepCharacter::Destroy()
{
	// give the money to the player now
	ATowerDefenseCharacter* MyCharacter = Cast<ATowerDefenseCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	MyCharacter->setCurrency(MyCharacter->getCurrency() + getMoneyValue());
	AController* myController = Controller;
	Controller->UnPossess();
	myController->Destroy();
	Super::Destroy();
}

void AEnemyTowerCreepCharacter::setAttackingTower(ABaseTower* tower) {
	this->attackingTower = tower;
}

float AEnemyTowerCreepCharacter::getHealth()
{
	return Health;
}

int AEnemyTowerCreepCharacter::getMoneyValue()
{
	return 30;
}

void AEnemyTowerCreepCharacter::Burn(){
	if (!Burnstatus)
	{
		if (burn)
		{
			UGameplayStatics::SpawnEmitterAttached(
				burn,                   //particle system
				RootComponent,      //mesh to attach to
				FName("Root"),   //socket name
				FVector(0, 0, 16),  //location relative to socket
				FRotator(0, 0, 0), //rotation 
				EAttachLocation::KeepRelativeOffset,
				true //will be deleted automatically
				);
			GetWorldTimerManager().SetTimer(this, &AEnemyTowerCreepCharacter::burnDamage, 1.0f, true, 0);
			Burnstatus = true;
		}
	}
}

void AEnemyTowerCreepCharacter::burnDamage()
{
	this->TakeDamage(10, FDamageEvent(), GetInstigatorController(), this);
}