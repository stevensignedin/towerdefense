// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "EnemyNexusCreepCharacter.h"
#include "NexusTurret.h"
#include "AIEnemyNexusCreepController.h"


AEnemyNexusCreepCharacter::AEnemyNexusCreepCharacter(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{
	AIControllerClass = AAIEnemyNexusCreepController::StaticClass();

	Health = baseHealth = 100.0f;
	AttackRange = 300.0f;
	Damage = 10.0f;
	AttackRate = 1.0f;
	Dead = false;
	Burnstatus = false;
}

void AEnemyNexusCreepCharacter::DealDamage()
{
	TArray<AActor*> output;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ANexusTurret::StaticClass(), output);
	if (output.IsValidIndex(0)) {
		ANexusTurret* nexus = (ANexusTurret*)output[0];
		if (nexus)
		{
			nexus->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
		}
	}
}
void AEnemyNexusCreepCharacter::StartAttack()
{
	//StopAnimMontage(AttackAnim);
	//AActor* PlayerCharacter = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (this)
		GetWorldTimerManager().SetTimer(this, &AEnemyNexusCreepCharacter::DealDamage, PlayAnimMontage(AttackAnim), true);
}

void AEnemyNexusCreepCharacter::StopAttack()
{
	StopAnimMontage(AttackAnim);
	//float timer = PlayAnimMontage(AttackAnim);
	GetWorld()->GetTimerManager().ClearTimer(this, &AEnemyNexusCreepCharacter::DealDamage);
}

float AEnemyNexusCreepCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

int AEnemyNexusCreepCharacter::getMoneyValue()
{
	return 20;
}

void AEnemyNexusCreepCharacter::Death()
{
	Super::Death();
	// standard death stuff
	StopAttack();
	Dead = true; 
	GetWorldTimerManager().SetTimer(this, &AEnemyNexusCreepCharacter::Destroy, PlayAnimMontage(DeathAnim) - 0.4, false);
}

void AEnemyNexusCreepCharacter::Destroy()
{
	// give the money to the player now
	ATowerDefenseCharacter* MyCharacter = Cast<ATowerDefenseCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	MyCharacter->setCurrency(MyCharacter->getCurrency() + getMoneyValue());
	AController* myController = Controller;
	Controller->UnPossess();
	myController->Destroy();
	Super::Destroy();
}


float AEnemyNexusCreepCharacter::getHealth()
{
	return Health;
}

void AEnemyNexusCreepCharacter::Burn(){
	if (!Burnstatus)
	{
		if (burn)
		{
			UGameplayStatics::SpawnEmitterAttached(
				burn,                   //particle system
				Mesh,      //mesh to attach to
				FName("Head"),   //socket name
				FVector(0, 0, 64),  //location relative to socket
				FRotator(0, 0, 0), //rotation 
				EAttachLocation::KeepRelativeOffset,
				true //will be deleted automatically
				);
			GetWorldTimerManager().SetTimer(this, &AEnemyNexusCreepCharacter::burnDamage, 1.0f, true, 0);
			Burnstatus = true;
		}
	}
}

void AEnemyNexusCreepCharacter::burnDamage()
{
	this->TakeDamage(20, FDamageEvent(), GetInstigatorController(), this);
}