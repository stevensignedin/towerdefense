// Fill out your copyright notice in the Description page of Project Settings.
#include "TowerDefense.h"
#include "AIBaseTowerController.h"
#include "Engine.h"
#include "Engine/Canvas.h"
#include "Engine/Font.h"
#include "BaseTower.h"
#include "TowerDefensePlayerController.h"
#include "TowerDefenseHUD.h"
#include "string.h"

int ABaseTower::minDistance = 350;

ABaseTower::ABaseTower(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{
	AIControllerClass = AAIBaseTowerController::StaticClass();

	occupied = false;
	alive = true;
	towerMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("TowerMesh"));
	RootComponent = towerMesh;

	cameraComponent = PCIP.CreateDefaultSubobject<UCameraComponent>(this, TEXT("TowerCamera"));
	cameraComponent->AttachTo(RootComponent);
	cameraComponent->RelativeLocation = FVector(0, 50, 850);
	cameraComponent->bUsePawnControlRotation = true;

	// now calculate the projectile offset
	// get the direction of the camera
	// add a bit for a distance offset to its y
	// so that the projectile sticks out a bit from the direction that the camera is facing
	
	// get the rotation of this vector
	FVector CameraLocation = cameraComponent->GetComponentLocation();
	const FRotator spawnRotation = cameraComponent->GetComponentLocation().Rotation();
	
	// add the offset
	// then push it back on

	projectileOffsetLength = 400.0f;
	projectileOffset = projectileOffsetLength*spawnRotation.Vector(); // the distance of the projectile from the camera

	ID = -1;
	baseHealth = Health = 100.0f;
	reachable = true;
	cost = 100; 
	myVT = FTViewTarget();
	myVT.POV.Rotation = FRotator(0, 90, 0);

	//	Get different health bars
	ConstructorHelpers::FObjectFinder<UBlueprint> ItemBlueprint(TEXT("Blueprint'/Game/Blueprints/HealthBarGood.HealthBarGood'"));
	if (ItemBlueprint.Object)
		barTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
	ItemBlueprint = ConstructorHelpers::FObjectFinder<UBlueprint>(TEXT("Blueprint'/Game/Blueprints/HealthBarCaution.HealthBarCaution'"));
	if (ItemBlueprint.Object)
		barTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
	ItemBlueprint = ConstructorHelpers::FObjectFinder<UBlueprint>(TEXT("Blueprint'/Game/Blueprints/HealthBarDanger.HealthBarDanger'"));
	if (ItemBlueprint.Object)
		barTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
}

void ABaseTower::Tick(float sec) {
	Super::Tick(sec);

	if (this || this->Controller != NULL)
		this->SpawnDefaultController();
}

void ABaseTower::spawnHealthBar() {
	if (healthBarClass) {
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		FRotator rote = FRotator(0, 0, 0);

		FVector loc = this->GetActorLocation();
		loc.Set(loc.X, loc.Y, loc.Z + 1000);

		myHealthBar = GetWorld()->SpawnActor<AHealthBar>(healthBarClass, loc, rote, spawnParams);
		myHealthBar->towerParent = this;
		myHealthBar->maxHealth = baseHealth;
	}
}

void ABaseTower::onFire(float currentPitch, float currentYawInput, float currentRoll) {
	if (projectileClass != NULL) {
		const FRotator spawnRotation = FRotator::FRotator(currentPitch, currentYawInput, currentRoll);// Calculate the pitch and the angle to fire projectile
		//FVector ActorLocation = RootComponent->GetComponentLocation();
		FVector CameraLocation = cameraComponent->GetComponentLocation();
		// FVector direction = cameraComponent->GetForwardVector();
		
		//float x = ActorLocation.X + CameraLocation.X;
		//float y = ActorLocation.Y + CameraLocation.Y;
		//float z = ActorLocation.Z + CameraLocation.Z;
		
		
		//float length = CameraLocation.Size();
		// CameraLocation.ToDirectionAndLength(CameraLocation, length);
		// cameraLocation is now a unitVector
		// length is the length of it
		// += 10.0f;
		// add the offset
		// then push it back on
		projectileOffset = projectileOffsetLength * spawnRotation.Vector();

		FVector loc = projectileOffset + CameraLocation; // this is the same as spawnRotation
		UWorld* const world = GetWorld();
		if (world != nullptr)
		{
			// reset the projectile offset to recalculate based upon camera's new position
			// FVector * outputTest = new FVector(80.0f, 90.0f, 100.0f);
			//FVector FiringStartLocation = spawnLocation;
			// FString tooutput = FString::Printf(TEXT("%d, %d, %d"), outputTest->X, outputTest->Y, outputTest->Z); 
			// if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, *tooutput);
			ATowerProjectile* const projectile = world->SpawnActor<ATowerProjectile>(projectileClass, loc, spawnRotation);
		}
	}
}

float ABaseTower::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.0f)
	{
		Health -= ActualDamage;

		//	Check if need different colored health bar
		if (Health / myHealthBar->maxHealth < 0.33) {
			//	Red health bar
			healthBarClass = barTypes[2];

			//	Destroy old health bar and replace
			myHealthBar->Destroy();

			if (this)
				this->spawnHealthBar();
		}
		else if (Health / myHealthBar->maxHealth < 0.66) {
			//	Yellow health bar
			healthBarClass = barTypes[1];

			//	Destroy old health bar and replace
			myHealthBar->Destroy();

			if (this)
				this->spawnHealthBar();
		}
	}

	if (Health <= 0.0f)
	{
		Death();
	}
	return ActualDamage;
}
void ABaseTower::enter()
{
	occupied = true;
}

void ABaseTower::exit()
{
	occupied = false;
}
bool ABaseTower::isOccupied()
{
	return occupied;
}
float ABaseTower::getHealth()
{
	return Health;
}

void ABaseTower::setHealth(float nhealth)
{
	Health = nhealth;
}

void ABaseTower::Death()
{
	alive = false;
	bCanBeDamaged = false;

	if (occupied) {
		ATowerDefensePlayerController* MyPlayerController = (ATowerDefensePlayerController*)UGameplayStatics::GetPlayerController(GetWorld(), 0);
		MyPlayerController->makeFinished();
	}
	else if (rubbleToSpawn) {
		//	Spawn rubble at location
		FVector loc = this->GetActorLocation();

		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		FRotator Rotation(0.0f, 0.0f, 0.0f);

		GetWorld()->SpawnActor<ARock>(rubbleToSpawn, loc, Rotation, spawnParams);
	}

	cameraComponent->DestroyComponent();
	this->myHealthBar->Destroy();
	Destroy();
}
