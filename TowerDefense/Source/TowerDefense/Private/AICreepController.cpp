// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "AICreepController.h"


AAICreepController::AAICreepController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}

//	Immediately run to target
void AAICreepController::BeginPlay() {
	Super::BeginPlay();
	CurrentState = AICreepControllerState::Start;
}

void AAICreepController::Tick(float Seconds) {
	Super::Tick(Seconds);
	
	if (!myCreep) {
		myCreep = (AEnemyCharacter*) this->GetPawn();
		return;
	}

	//	Check if hit by a tower
	/*TArray<AActor*> overlaps;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerProjectile::StaticClass(), overlaps);
	if (overlaps.Num() > 0)
	{
		
		for (int i = 0; i < overlaps.Num(); i++)
		{
			ATowerProjectile* projectile = Cast<ATowerProjectile>(overlaps[i]);
			if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("distance to projectile is %d"), projectile->GetDistanceTo(this)));
			if (projectile->GetDistanceTo(this) < 1000)
			{
				
				// take 2 dmg
				if (this->GetPawn())
				{
					if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("taking damage") ) );
					this->GetPawn()->TakeDamage(.25f, FDamageEvent(), GetInstigatorController(), projectile);
				}
					
			}
		}
	}*/
}

void AAICreepController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) {
	// doesnt work for some reason
	// so ignore
	/*CurrentState = AICreepControllerState::AttackTarget;
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("In Move Completed, should start attack!!!")) );

	if (myCreep)
		myCreep->StartAttack();*/
}