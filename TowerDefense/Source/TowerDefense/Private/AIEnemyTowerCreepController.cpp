// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "TowerDefensePlayerController.h"
#include "AIEnemyTowerCreepController.h"
#include "BaseTower.h"


AAIEnemyTowerCreepController::AAIEnemyTowerCreepController(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP) {}

//	Immediately go for nearest tower
void AAIEnemyTowerCreepController::BeginPlay()
{
	Super::BeginPlay();
	CurrentState = AICreepControllerState::NoTarget;
}

void AAIEnemyTowerCreepController::Tick(float Seconds)
{

	Super::Tick(Seconds);

	//	Set myCreep
	if (!myCreep) {
		myCreep = (AEnemyTowerCreepCharacter*) this->GetPawn();
		return;
	}

	//	Don't do anything if dead
	if (CurrentState == AICreepControllerState::Dead)
		return;
	
	//	Check if killed
	if (((AEnemyTowerCreepCharacter*)myCreep)->getHealth() <= 0)
	{
		this->StopMovement();
		((AEnemyTowerCreepCharacter*)myCreep)->Death();

		CurrentState = AICreepControllerState::Dead;
	}

	//	Check run state
	if (CurrentState == AICreepControllerState::RunToTarget) {

		//	Check if tower is destroyed
		ABaseTower* atk = ((AEnemyTowerCreepCharacter*)myCreep)->attackingTower;
		if (atk && (atk->IsPendingKill() || atk->IsPendingKillPending()))
			CurrentState = AICreepControllerState::NoTarget;
	}
	
	else if (CurrentState == AICreepControllerState::AttackTarget)
	{
		//	Check if tower is destroyed
		ABaseTower* atk = ((AEnemyTowerCreepCharacter*)myCreep)->attackingTower;
		if (atk && atk->IsPendingKill()) {
			myCreep->StopAttack();
			CurrentState = AICreepControllerState::NoTarget;
		}
	}
	

	//	Check bored state
	else if (CurrentState == AICreepControllerState::NoTarget) {
		//	Find a target
		ABaseTower* tower = nextTarget();
		if (tower != nullptr) {
			//	Attack the tower
			CurrentState = AICreepControllerState::RunToTarget;
			((AEnemyTowerCreepCharacter*)myCreep)->setAttackingTower(tower);

			MoveToActor(tower, -1.0f, false, false);
		}
	}
}

//	Find the nearest tower to attack, if there is one
ABaseTower* AAIEnemyTowerCreepController::nextTarget()
{
	TArray<AActor*> actor;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), actor);

	if (actor.Num() > 0) {
		ATowerManager* manager = (ATowerManager*)actor[0];

		if (manager) {
			manager->updateTowerList();

			TArray<ABaseTower*> towers = manager->allTowers;
			if (towers.Num() > 0) {
				ABaseTower* closest = (ABaseTower*)towers[0];
				if (closest == ((AEnemyTowerCreepCharacter*)myCreep)->attackingTower || !closest->reachable) {
					if (towers.Num() > 1)
						closest = (ABaseTower*)towers[1];
					else
						closest = nullptr;
				}

				for (int i = 1; i < towers.Num(); i++)
				{
					//	Valid if not currently attacking tower and tower is reachable
					if (towers[i]->GetDistanceTo(this) < closest->GetDistanceTo(this) &&
						((ABaseTower*)towers[i]) != ((AEnemyTowerCreepCharacter*)myCreep)->attackingTower &&
							(ABaseTower*)towers[i]->reachable)
						closest = (ABaseTower*)towers[i];
				}

				return closest;
			}
		}
	}

	return nullptr;
}

//	Called once tower to attack has been reached
void AAIEnemyTowerCreepController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	if (myCreep && Result == EPathFollowingResult::Type::Success)
	{
		CurrentState = AICreepControllerState::AttackTarget;
		myCreep->StartAttack();
	}

	//	Pathing failed
	else if (Result != EPathFollowingResult::Type::Aborted) {
		CurrentState = AICreepControllerState::NoTarget;

		//	Mark the tower as unreachable
		TArray<AActor*> actor;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), actor);

		if (actor.Num() > 0) {
			ATowerManager* manager = (ATowerManager*)actor[0];
			if (manager) {
				// if (!manager->markUnreachable(((AEnemyTowerCreepCharacter*)myCreep)->attackingTower))
					// ATowerDefensePlayerController::printMessage("FAILED TO MARK AS UNREACHABLE");
			}
		}
	}
}