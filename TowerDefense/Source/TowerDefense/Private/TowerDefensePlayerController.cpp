// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#include "TowerDefense.h"
#include "AIBaseTowerController.h"
#include "BuildModeCameraManager.h"
#include "TowerDefensePlayerController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "TowerDefenseHUD.h"

ATowerDefensePlayerController::ATowerDefensePlayerController(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	autoFireRate = 0.2f;
	towerOccupied = false;
	lookingLeft = false;
	lookingRight = false;
	start = true;
	inBuildMode = true;
	this->PlayerCameraManagerClass = ATowerPlayerCameraManager::StaticClass();
	

	ConstructorHelpers::FObjectFinder<UBlueprint> ItemBlueprint(TEXT("Blueprint'/Game/Blueprints/BaseTower.BaseTower'"));
	if (ItemBlueprint.Object){
		towerTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
	}
	ItemBlueprint = ConstructorHelpers::FObjectFinder<UBlueprint>(TEXT("Blueprint'/Game/Blueprints/IceTower.IceTower'"));
	if (ItemBlueprint.Object){
		towerTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
	}
	ItemBlueprint = ConstructorHelpers::FObjectFinder<UBlueprint>(TEXT("Blueprint'/Game/Blueprints/FireTower.FireTower'"));
	if (ItemBlueprint.Object){
		towerTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
	}
	towerToBuild = towerTypes[0]; 
}
void ATowerDefensePlayerController::baseTowerType()
{
	towerToBuild = towerTypes[0];
}
void ATowerDefensePlayerController::iceTowerType()
{
	towerToBuild = towerTypes[1];
}
void ATowerDefensePlayerController::fireTowerType()
{
	towerToBuild = towerTypes[2];
}
void ATowerDefensePlayerController::makeFinished()
{
	((ATowerDefenseHUD*)GetHUD())->lostGame = true;
	// when this flag is switched, then the HUD (which updates every tick i think?
	// calls endGame. endGame makes it so that the game is paused and the game is over
}

void ATowerDefensePlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
	TArray<AActor*> output;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), output);
	if (output.Num() > 0) {
		((ATowerManager*)output[0])->towerToSpawn = towerToBuild;

		if (!myPlayer) {
			APawn* const pawn = GetPawn();
			if (pawn)
				myPlayer = (ATowerDefenseCharacter*)pawn;

			return;
		}

		if (!mySoundBox) {
			TArray<AActor*> output;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASoundBox::StaticClass(), output);
			if (output.Num() > 0)
				mySoundBox = (ASoundBox*)output[0];

			return;
		}

		if (start)
		{
			// THIS STARTS YOU IN BUILD MODE
			// uncomment this line for profit
			this->buildMode();
			start = false;

			//	Start background music
			mySoundBox->playBackgroundMusic();

			return;
		}

		//	Show da tower you gon build at mouse location
		if (inBuildMode) {

			//	Get mouse cursor target in world
			FHitResult Hit;
			GetHitResultUnderCursor(ECC_Visibility, true, Hit);
			if (Hit.bBlockingHit) {
				FActorSpawnParameters spawnParams;
				spawnParams.Owner = this;
				spawnParams.Instigator = Instigator;
				FRotator Rotation(0.0f, 0.0f, 0.0f);
				if (!myGhostTower) {
					//	Spawn the ghost tower at the start
					TArray<AActor*> output;
					UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), output);
					if (output[0]) {
						myGhostTower = GetWorld()->SpawnActor<ABaseTower>(((ATowerManager*)output[0])->towerToSpawn, Hit.Location, Rotation, spawnParams);
						myGhostTower->SpawnDefaultController();
						((AAIBaseTowerController*)(myGhostTower->GetController()))
							->setCurrentState(BaseTowerControllerState::Ghost);	//	Ghost state means controller doesn't do anything
						myGhostTower->SetActorEnableCollision(false);	//	Hidden from ray tracing
					}
				}
				else {
					TArray<AActor*> output;
					UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), output);
					bool hide = false;
					if (output[0]) {
						ATowerManager* manager = (ATowerManager*)output[0];

						if (manager) {
							manager->updateTowerList();
							TArray<ABaseTower*> towers = manager->allTowers;

							AActor* actor = Hit.Actor.Get(true);
							ABaseTower* checkHitTower = Cast<ABaseTower>(actor);

							//	Check all towers
							for (int i = 0; i < towers.Num(); i++) {
								ABaseTower* tower = (ABaseTower*)towers[i];
								if (tower) {
									//	Hide ghost if under minDistance of another tower
									if (FVector::Dist(Hit.Location, tower->GetActorLocation()) <= ABaseTower::minDistance) {
										myGhostTower->SetActorHiddenInGame(true);
										hide = true;
										break;
									}

									//	Also hide if on top of a tower (distance could still be greater than minDistance)
									if (checkHitTower == tower) {
										myGhostTower->SetActorHiddenInGame(true);
										hide = true;
										break;
									}
								}
							}

							//	And hide if on a health bar
							AHealthBar* bar = Cast<AHealthBar>(actor);
							if (bar) {
								myGhostTower->SetActorHiddenInGame(true);
								hide = true;
							}
						}
					}

					if (!hide)
						myGhostTower->SetActorHiddenInGame(false);

					myGhostTower->SetActorLocation(Hit.Location);
				}

				if (!myGhostTower->IsA(towerToBuild))
				{
					myGhostTower->Destroy();
					GetWorld()->DestroyActor(myGhostTower);
					myGhostTower = NULL;
				}
			}
		}
	}
}

void ATowerDefensePlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	//	Make sure game starts from scratch
	UInputSettings* uis = GetMutableDefault<UInputSettings>();
	uis = removeRelevantMappings(uis);

	//	Add build mode mapping
	FInputActionKeyMapping map(FName("Build Mode"), FKey(FName("B")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	//	Add fire mode mapping
	map = FInputActionKeyMapping(FName("Fire Mode"), FKey(FName("Enter")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);
	((UInputSettings*)uis)->SaveKeyMappings();

	//	Add bindings for build and fire
	InputComponent->BindAction("Build Mode", IE_Pressed, this, &ATowerDefensePlayerController::buildMode);
	InputComponent->BindAction("Fire Mode", IE_Pressed, this, &ATowerDefensePlayerController::fireMode);

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATowerDefensePlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ATowerDefensePlayerController::OnSetDestinationReleased);
	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATowerDefensePlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ATowerDefensePlayerController::MoveToTouchLocation);
}

//  Panning the tower camera
void ATowerDefensePlayerController::lookLeft()  { lookingLeft = true; }
void ATowerDefensePlayerController::stopLeft()  { lookingLeft = false; }
void ATowerDefensePlayerController::lookRight() { lookingRight = true; }
void ATowerDefensePlayerController::stopRight() { lookingRight = false; }

//  Panning the build camera
void ATowerDefensePlayerController::panLeft()	{ panningLeft = true; }
void ATowerDefensePlayerController::panRight()  { panningRight = true; }
void ATowerDefensePlayerController::panUp()		{ panningUp = true; }
void ATowerDefensePlayerController::panDown()	{ panningDown = true; }

void ATowerDefensePlayerController::stopPanLeft()	{ panningLeft = false; }
void ATowerDefensePlayerController::stopPanRight()  { panningRight = false; }
void ATowerDefensePlayerController::stopPanUp()		{ panningUp = false; }
void ATowerDefensePlayerController::stopPanDown()	{ panningDown = false; }

//	Zooming the build camera
void ATowerDefensePlayerController::zoomOut()	{ zoomingOut = true; }
void ATowerDefensePlayerController::zoomIn()	{ zoomingIn = true; }

//	Fire a projectile from tower
void ATowerDefensePlayerController::playerFire() {
	if (myPlayer && towerOccupied) {
		FVector target, directionOfTarget;

		//	Get mouse cursor target in world
		if (this->DeprojectMousePositionToWorld(target, directionOfTarget))
		{
			static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
			FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
			TraceParams.bTraceAsyncScene = true;
			TraceParams.bReturnPhysicalMaterial = true;
			FHitResult Hit(ForceInit);
			FVector myLocation = myPlayer->myTower->cameraComponent->GetComponentLocation();
			GetWorld()->LineTraceSingle(Hit, myLocation, target, TraceParams,
				FCollisionObjectQueryParams::AllObjects);
			ABaseTower* tower = Cast<ABaseTower>(Hit.GetActor()); 
			if (!tower)
			{
				myPlayer->myTower->onFire((directionOfTarget.Rotation()).Pitch, (directionOfTarget.Rotation()).Yaw, (directionOfTarget.Rotation()).Roll);
			}
		}
	}
}

void ATowerDefensePlayerController::autoFire() {
	if (this)
		GetWorldTimerManager().SetTimer(this, &ATowerDefensePlayerController::playerFire, autoFireRate, true, 0);
}

void ATowerDefensePlayerController::stopAutoFire() {
	if (this)
		GetWorldTimerManager().ClearTimer(this, &ATowerDefensePlayerController::playerFire);
}

//	Enter the game's build mode
void ATowerDefensePlayerController::buildMode() {

	//	Set tower's occupied to false if you were in it
	if (towerOccupied) {
		myPlayer->exitTower();
		towerOccupied = false;
	}

	setBuildMappings();
	//	Get the build camera actor
	TArray<AActor*> output;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildModeCamera::StaticClass(), output);
	if (output[0]) {
		ABuildModeCamera* buildCam = (ABuildModeCamera*)output[0];

		//	Remove all camera managers
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerPlayerCameraManager::StaticClass(), output);
		if (output.Num() > 0) {
			for (int i = 0; i < output.Num(); i++) {
				output[i]->RemoveFromRoot();
				output[i]->Destroy();
			}
		}
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildModeCameraManager::StaticClass(), output);
		if (output.Num() > 0) {
			for (int i = 0; i < output.Num(); i++) {
				output[i]->RemoveFromRoot();
				output[i]->Destroy();
			}
		}

		this->PlayerCameraManagerClass = ABuildModeCameraManager::StaticClass();
		this->SpawnPlayerCameraManager();
		this->PlayerCameraManager->bFindCameraComponentWhenViewTarget = true;
		this->SetViewTarget(buildCam);
	}
}

//	Occupy (as of now) the first tower in memory
void ATowerDefensePlayerController::fireMode() {
	setFireMappings();
	TArray<AActor*> managers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), managers);
	int32 numTowers; 
	ATowerManager* towerManager = (ATowerManager*)((managers[0])); 
	TArray<ABaseTower*> towersInGame = towerManager->allTowers;
	numTowers = towersInGame.Num(); 
	if (myPlayer && (numTowers > 0)) {
		myPlayer->enterTower();

		//	Remove all camera managers
		TArray<AActor*> output;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildModeCameraManager::StaticClass(), output);
		if (output.Num() > 0) {
			for (int i = 0; i < output.Num(); i++) {
				output[i]->RemoveFromRoot();
				output[i]->Destroy();
			}
		}
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerPlayerCameraManager::StaticClass(), output);
		if (output.Num() > 0) {
			for (int i = 0; i < output.Num(); i++) {
				output[i]->RemoveFromRoot();
				output[i]->Destroy();
			}
		}

		this->PlayerCameraManagerClass = ATowerPlayerCameraManager::StaticClass();
		this->SpawnPlayerCameraManager();
		this->PlayerCameraManager->bFindCameraComponentWhenViewTarget = true;
		this->SetViewTarget(myPlayer->myTower);
		towerOccupied = true;
	}
}

void ATowerDefensePlayerController::setFireMappings() {
	inBuildMode = false;

	//	Get action mappings
	UInputSettings* uis = GetMutableDefault<UInputSettings>();

	uis = removeRelevantMappings(uis);

	//	Add build mode binding
	FInputActionKeyMapping map(FName("Build Mode"), FKey(FName("SpaceBar")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);
		
	InputComponent->BindAction("Build Mode", IE_Pressed, this,
		&ATowerDefensePlayerController::buildMode);

	//	Add left click binding
	map = FInputActionKeyMapping(FName("Left Click"), FKey(FName("LeftMouseButton")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("Left Click", IE_Pressed, this,
		&ATowerDefensePlayerController::autoFire);
	InputComponent->BindAction("Left Click", IE_Released, this,
		&ATowerDefensePlayerController::stopAutoFire);

	//	Add Q binding
	map = FInputActionKeyMapping(FName("Q Key"), FKey(FName("Q")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);
	InputComponent->BindAction("Q Key", IE_Pressed, this,
		&ATowerDefensePlayerController::nextTower);

	//	Add E binding
	map = FInputActionKeyMapping(FName("E Key"), FKey(FName("E")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);
	InputComponent->BindAction("E Key", IE_Pressed, this,
		&ATowerDefensePlayerController::previousTower);

	//	Add A binding
	map = FInputActionKeyMapping(FName("A Key"), FKey(FName("A")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("A Key", IE_Pressed, this,
		&ATowerDefensePlayerController::lookLeft);
	InputComponent->BindAction("A Key", IE_Released, this,
		&ATowerDefensePlayerController::stopLeft);

	//	Add D binding
	map = FInputActionKeyMapping(FName("D Key"), FKey(FName("D")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);
	((UInputSettings*)uis)->SaveKeyMappings();

	InputComponent->BindAction("D Key", IE_Pressed, this,
		&ATowerDefensePlayerController::lookRight);
	InputComponent->BindAction("D Key", IE_Released, this,
		&ATowerDefensePlayerController::stopRight);


	//	Add W binding
	map = FInputActionKeyMapping(FName("W Key"), FKey(FName("W")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("W Key", IE_Pressed, this,
		&ATowerDefensePlayerController::panUp);
	InputComponent->BindAction("W Key", IE_Released, this,
		&ATowerDefensePlayerController::stopPanUp);

	//	Add S binding
	map = FInputActionKeyMapping(FName("S Key"), FKey(FName("S")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);


	InputComponent->BindAction("S Key", IE_Pressed, this,
		&ATowerDefensePlayerController::panDown);
	InputComponent->BindAction("S Key", IE_Released, this,
		&ATowerDefensePlayerController::stopPanDown);

	((UInputSettings*)uis)->SaveKeyMappings();
}

void ATowerDefensePlayerController::setBuildMappings() {
	inBuildMode = true;

	//	Get action mappings
	UInputSettings* uis = GetMutableDefault<UInputSettings>();

	uis = removeRelevantMappings(uis);

	//	Add fire mode binding
	FInputActionKeyMapping map(FName("Fire Mode"), FKey(FName("SpaceBar")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("Fire Mode", IE_Pressed, this,
		&ATowerDefensePlayerController::fireMode);

	//	Add left click binding
	map = FInputActionKeyMapping(FName("Left Click"), FKey(FName("LeftMouseButton")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("Left Click", IE_Pressed, this,
		&ATowerDefensePlayerController::placeTower);

	//	Add A binding
	map = FInputActionKeyMapping(FName("A Key"), FKey(FName("A")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("A Key", IE_Pressed, this,
		&ATowerDefensePlayerController::panLeft);
	InputComponent->BindAction("A Key", IE_Released, this,
		&ATowerDefensePlayerController::stopPanLeft);

	//	Add D binding
	map = FInputActionKeyMapping(FName("D Key"), FKey(FName("D")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("D Key", IE_Pressed, this,
		&ATowerDefensePlayerController::panRight);
	InputComponent->BindAction("D Key", IE_Released, this,
		&ATowerDefensePlayerController::stopPanRight);

	//	Add W binding
	map = FInputActionKeyMapping(FName("W Key"), FKey(FName("W")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("W Key", IE_Pressed, this,
		&ATowerDefensePlayerController::panUp);
	InputComponent->BindAction("W Key", IE_Released, this,
		&ATowerDefensePlayerController::stopPanUp);

	//	Add S binding
	map = FInputActionKeyMapping(FName("S Key"), FKey(FName("S")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("S Key", IE_Pressed, this,
		&ATowerDefensePlayerController::panDown);
	InputComponent->BindAction("S Key", IE_Released, this,
		&ATowerDefensePlayerController::stopPanDown);

	//	Add zoom out binding
	map = FInputActionKeyMapping(FName("Zoom Out"), FKey(FName("MouseScrollDown")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("Zoom Out", IE_Pressed, this,
		&ATowerDefensePlayerController::zoomOut);

	//	Add zoom in binding
	map = FInputActionKeyMapping(FName("Zoom In"), FKey(FName("MouseScrollUp")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);

	InputComponent->BindAction("Zoom In", IE_Pressed, this,
		&ATowerDefensePlayerController::zoomIn);

	map = FInputActionKeyMapping(FName("Base Tower"), FKey(FName("One")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);
	InputComponent->BindAction("Base Tower", IE_Pressed, this,
		&ATowerDefensePlayerController::baseTowerType);

	map = FInputActionKeyMapping(FName("Ice Tower"), FKey(FName("Two")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);
	InputComponent->BindAction("Ice Tower", IE_Pressed, this,
		&ATowerDefensePlayerController::iceTowerType);

	map = FInputActionKeyMapping(FName("Fire Tower"), FKey(FName("Three")), false, false, false, false);
	((UInputSettings*)uis)->AddActionMapping(map);
	InputComponent->BindAction("Fire Tower", IE_Pressed, this,
		&ATowerDefensePlayerController::fireTowerType);


	((UInputSettings*)uis)->SaveKeyMappings();
}

UInputSettings* ATowerDefensePlayerController::removeRelevantMappings(UInputSettings* uis) {
	TArray<FInputActionKeyMapping> mappings = uis->ActionMappings;

	//	Remove left click, WASD, QE, build, and fire
	for (int i = 0; i < mappings.Num(); i++) {
		//	Build Mode
		if (mappings[i].ActionName.Compare(FName::FName("Build Mode")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	Fire Mode
		if (mappings[i].ActionName.Compare(FName::FName("Fire Mode")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	Zoom in
		if (mappings[i].ActionName.Compare(FName::FName("Zoom In")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	Zoom out
		if (mappings[i].ActionName.Compare(FName::FName("Zoom Out")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	Left click
		if (mappings[i].ActionName.Compare(FName::FName("Left Click")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	Q key
		if (mappings[i].ActionName.Compare(FName::FName("Q Key")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	E key
		if (mappings[i].ActionName.Compare(FName::FName("E Key")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	A key
		if (mappings[i].ActionName.Compare(FName::FName("A Key")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	D key
		if (mappings[i].ActionName.Compare(FName::FName("D Key")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	W key
		if (mappings[i].ActionName.Compare(FName::FName("W Key")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}

		//	S key
		if (mappings[i].ActionName.Compare(FName::FName("S Key")) == 0) {
			FInputActionKeyMapping map = mappings[i];
			((UInputSettings*)uis)->RemoveActionMapping(map);
		}
	}

	InputComponent->ClearActionBindings();

	return uis;
}

//	Place tower at mouse location
void ATowerDefensePlayerController::placeTower() {
	//	If clicked a tower, occupy it and go to fire mode
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, true, Hit);
	if (Hit.bBlockingHit) {
		AActor* actor = Hit.Actor.Get(true);
		ABaseTower* tower = Cast<ABaseTower>(actor);

		//	Did click a tower
		if (tower) {
			setFireMappings();
			myPlayer->enterTower(tower);

			//	This part copied from fireMode()
			//	Remove all camera managers
			TArray<AActor*> output;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildModeCameraManager::StaticClass(), output);
			if (output.Num() > 0) {
				for (int i = 0; i < output.Num(); i++) {
					output[i]->RemoveFromRoot();
					output[i]->Destroy();
				}
			}
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerPlayerCameraManager::StaticClass(), output);
			if (output.Num() > 0) {
				for (int i = 0; i < output.Num(); i++) {
					output[i]->RemoveFromRoot();
					output[i]->Destroy();
				}
			}

			this->PlayerCameraManagerClass = ATowerPlayerCameraManager::StaticClass();
			this->SpawnPlayerCameraManager();
			this->PlayerCameraManager->bFindCameraComponentWhenViewTarget = true;
			this->SetViewTarget(myPlayer->myTower);
			towerOccupied = true;
			
			return;
		}

		//	Also check if clicked tower health bar
		AHealthBar* bar = Cast<AHealthBar>(actor);
		if (bar)
			return;
	}

	//	If clicked within minDistance of another tower, can't build
	TArray<AActor*> output;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), output);
	bool hide = false;
	if (output[0]) {
		ATowerManager* manager = (ATowerManager*)output[0];

		if (manager) {
			manager->updateTowerList();
			TArray<ABaseTower*> towers = manager->allTowers;

			//	Check all towers
			for (int i = 0; i < towers.Num(); i++) {
				ABaseTower* tower = (ABaseTower*)towers[i];
				if (tower) {
					//	Return if within minDistance
					if (FVector::Dist(Hit.Location, tower->GetActorLocation()) <= ABaseTower::minDistance) {
						return;
					}
				}
			}
		}
	}

	//	Make sure player can afford tower
	int32 cost; 
	if (myGhostTower->IsA(towerTypes[0]))
	{
		cost = 100; 
	}
	else
	{
		cost = 150; 
	}
	if (myPlayer) {
		if (myPlayer->getCurrency() >= cost) {
			//	Deduct cost and spawn
			myPlayer->addCurrency((-1)*cost);

			if (Hit.bBlockingHit) {
				TArray<AActor*> output;
				UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATowerManager::StaticClass(), output);
				if (output[0]) {
					//((ATowerManager*)output[0])->towerToSpawn = towerToBuild; 
					((ATowerManager*)output[0])->spawnTower(Hit.ImpactPoint);
				}
			}
		}
		else {
			//	Can't afford
			printMessage("YOU CAN'T AFFORD THE TOWER! :(");
		}
	}
}

//	Switch to the next tower in memory
void ATowerDefensePlayerController::nextTower(){
	if (myPlayer) {
		((ATowerPlayerCameraManager*)PlayerCameraManager)->toggleTowerChanged();
		myPlayer->nextTower();
		this->PlayerCameraManager->bFindCameraComponentWhenViewTarget = 1;
		this->SetViewTarget(myPlayer->myTower);
		towerOccupied = true;
	}
}

void ATowerDefensePlayerController::previousTower()
{
	if (myPlayer)
	{
		((ATowerPlayerCameraManager*)PlayerCameraManager)->toggleTowerChanged();
		myPlayer->previousTower();
		this->PlayerCameraManager->bFindCameraComponentWhenViewTarget = 1;
		this->SetViewTarget(myPlayer->myTower);
		towerOccupied = true;
	}
}

void ATowerDefensePlayerController::MoveToMouseCursor()
{
	// Trace to see what is under the mouse cursor
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	if (Hit.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(Hit.ImpactPoint);
	}
}

void ATowerDefensePlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);
	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void ATowerDefensePlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const Pawn = GetPawn();
	if (Pawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(DestLocation, Pawn->GetActorLocation());
		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void ATowerDefensePlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ATowerDefensePlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void ATowerDefensePlayerController::printMessage(FString string) {
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, string);
}