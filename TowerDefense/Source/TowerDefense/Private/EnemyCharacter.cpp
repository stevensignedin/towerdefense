// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "ParticleDefinitions.h"
#include "EnemyCharacter.h"


AEnemyCharacter::AEnemyCharacter(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{
	Dead = false;
	Burnstatus = false; 
	Freezestatus = false;
	baseHealth = Health = 100.0f;

	//	Get different health bars
	ConstructorHelpers::FObjectFinder<UBlueprint> ItemBlueprint(TEXT("Blueprint'/Game/Blueprints/HealthBarGood.HealthBarGood'"));
	if (ItemBlueprint.Object)
		barTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
	ItemBlueprint = ConstructorHelpers::FObjectFinder<UBlueprint>(TEXT("Blueprint'/Game/Blueprints/HealthBarCaution.HealthBarCaution'"));
	if (ItemBlueprint.Object)
		barTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
	ItemBlueprint = ConstructorHelpers::FObjectFinder<UBlueprint>(TEXT("Blueprint'/Game/Blueprints/HealthBarDanger.HealthBarDanger'"));
	if (ItemBlueprint.Object)
		barTypes.Push((UClass*)ItemBlueprint.Object->GeneratedClass);
}
// float getHealth() { return 0.0f; }

void AEnemyCharacter::StartAttack() {}

void AEnemyCharacter::StopAttack() {}

void AEnemyCharacter::DealDamage() {}

float AEnemyCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.0f)
	{
		Health -= ActualDamage;

		//	Check if need different colored health bar
		if (Health / myHealthBar->maxHealth < 0.33) {
			//	Red health bar
			healthBarClass = barTypes[2];

			//	Destroy old health bar and replace
			myHealthBar->Destroy();

			if (this)
				this->spawnHealthBar();
		}
		else if (Health / myHealthBar->maxHealth < 0.66) {
			//	Yellow health bar
			healthBarClass = barTypes[1];

			//	Destroy old health bar and replace
			myHealthBar->Destroy();

			if (this)
				this->spawnHealthBar();
		}
	}
	if (Health <= 0.0f)
	{
		if (!Dead)
		{
			Dead = true;
		}
	}
	return ActualDamage;
}

void AEnemyCharacter::setHealth(float h) { this->Health = h; }

void AEnemyCharacter::Slow(){
	if (!Freezestatus) {
		this->CharacterMovement->MaxWalkSpeed *= .5;
		GetWorldTimerManager().SetTimer(this, &AEnemyCharacter::fixSpeed, 3.0f, false, -1);
		Freezestatus = true;
	}
}

void AEnemyCharacter::fixSpeed()
{
	this->CharacterMovement->MaxWalkSpeed *= 2;
	Freezestatus = false;
}

void AEnemyCharacter::Burn() {}

void AEnemyCharacter::burnDamage() {}

void AEnemyCharacter::Death() {
	myHealthBar->Destroy();
}

int AEnemyCharacter::getMoneyValue(){ return 10; }

void AEnemyCharacter::spawnHealthBar() {
	if (healthBarClass) {
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		FRotator rote = FRotator(0, 0, 0);

		FVector loc = this->GetActorLocation();
		loc.Set(loc.X, loc.Y, loc.Z + 200);
		if (Health > 0.0f) {
			myHealthBar = GetWorld()->SpawnActor<AHealthBar>(healthBarClass, loc, rote, spawnParams);
			myHealthBar->charParent = this;
			myHealthBar->maxHealth = baseHealth;
		}
	}
}