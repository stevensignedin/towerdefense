// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "AIBaseTowerController.h"


AAIBaseTowerController::AAIBaseTowerController(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{
	attackRange = 10000.0f;

	attackRate = 1.0f;	//	Once per sec
}

void AAIBaseTowerController::BeginPlay() {
	Super::BeginPlay();
	currentState = BaseTowerControllerState::NoTarget;
}

void AAIBaseTowerController::Tick(float sec) {
	Super::Tick(sec);

	//	Set myTower
	if (!myTower) {
		myTower = (ABaseTower*) this->GetPawn();
		return;
	}

	//	Check if occupied or not first
	if (myTower->isOccupied() && currentState != BaseTowerControllerState::Occupied) {
		currentState = BaseTowerControllerState::Occupied;
		this->stopAttack();
		return;
	}

	if (!myTower->isOccupied() && currentState == BaseTowerControllerState::Occupied) {
		currentState = BaseTowerControllerState::NoTarget;
	}

	//	Find a target, if within range
	if (currentState == BaseTowerControllerState::NoTarget) {
		//	Get all enemies in da world
		creepTarget = getClosestEnemy();
		startAttack();
		currentState = BaseTowerControllerState::AttackingTarget;
	}

	//	Check if target r ded or out of range
	if (currentState == BaseTowerControllerState::AttackingTarget) {
		//	Out of range
		// FVector inFrontOf = FVector(0, 1, 0); // this is the vector in front of everyone
		// only attack once

		// then reevaluate if it should attack it

		//currentState = BaseTowerControllerState::NoTarget;
		//stopAttack();
		
		
		
		

		// reevaluate closest target
		creepTarget = getClosestEnemy();
		if (creepTarget)
		{
			// if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("valid target exists") ) );
			
			if (myTower->GetDistanceTo(creepTarget) > attackRange)
			{
				currentState = BaseTowerControllerState::NoTarget;
				stopAttack();
			}
		}
		else if (!creepTarget || creepTarget->IsPendingKill())
		{
			// we killed it team
			// we did it
			currentState = BaseTowerControllerState::NoTarget;
			
			stopAttack();
			// if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("valid target died") ) );
		}
	}
}

AEnemyCharacter* AAIBaseTowerController::getClosestEnemy()
{
	TArray<AActor*> creeps;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyCharacter::StaticClass(), creeps);

	if (creeps.Num() > 0) {
		AEnemyCharacter* closest = (AEnemyCharacter*)creeps[0];

		//	Find closest creep to pew
		for (int i = 1; i < creeps.Num(); i++) {
			// FVector enemyLocation = creeps[i]->GetActorLocation();

			FVector startPos = myTower->cameraComponent->RelativeLocation + myTower->projectileOffset - (0, 0, 400);
			FVector enemyLocationWillBeIn = getVectorToAimIn(closest);
			// we want to draw a line from 
			// FVector fireDirectionVector = targetPos - startPos;
			// float theDot = FVector::DotProduct(targetPos, startPos);
			static FName TowerTag = FName(TEXT("TowerTrace"));
			FCollisionQueryParams TraceParams(TowerTag, true, Instigator);
			TraceParams.bTraceAsyncScene = true;
			TraceParams.bReturnPhysicalMaterial = true;

			// This fires the ray and checks against all objects w/ collision
			// pew pew pew
			FHitResult Hit(ForceInit);
			GetWorld()->LineTraceSingle(Hit, startPos, enemyLocationWillBeIn, TraceParams, FCollisionObjectQueryParams::AllObjects);
			// Did this hit anything?

			if (!Hit.bBlockingHit)
			{



				if (myTower->GetDistanceTo(closest) > myTower->GetDistanceTo(creeps[i]))
				{
					// get the closest out of these
					closest = (AEnemyCharacter*)creeps[i];
				}
			}
			else
			{

				// something is blocking us!!!
				// if an enemy is blocking us
				// then we would pick that enemy as closest....

				// but still accounting for it i guess....

				// if it is an enemy we don't care
				AEnemyCharacter* test = static_cast<AEnemyCharacter*>(Hit.GetActor());

				if (test != 0) {
					// there is another enemy in the way

					// owelll fire!!!! anyways.
					if (myTower->GetDistanceTo(closest) > myTower->GetDistanceTo(creeps[i]))
					{
						// get the closest out of these
						closest = (AEnemyCharacter*)creeps[i];
					}
				}



			}

		}

		return closest; 
	}
	return nullptr;
}
void AAIBaseTowerController::attack()
{

	//	THIS IS THE HARD PART GOOD NIGHT
	if (creepTarget) // this is the one we want to hit
	{
		

		FVector SpotToAimInVector = getVectorToAimIn(creepTarget);
		// do the climactic pew pew

		myTower->onFire(SpotToAimInVector.Rotation().Pitch, SpotToAimInVector.Rotation().Yaw, SpotToAimInVector.Rotation().Roll);
	}
		/*FVector targetPos = creepTarget->GetActorLocation();
		
		FVector fireDirectionVector = targetPos - startPos;
		// get the rotation of this vector
		FRotator rot = fireDirectionVector.Rotation();
		// i use the rot to get the unit vector
		// you could also use toDirectionAndLength but y wud u lol
		myTower->projectileOffset = myTower->projectileOffsetLength*rot.Vector();
		// move it so it lines up the thing properly
		// if you don't reevaluate the projectileOffset, then it constantly fires from the same position
		// which is exactly in front of the tower.

		//DrawDebugLine(GetWorld(), startPos, targetPos, FColor::Red, true, 10.0f);
		static FName TowerTag = FName(TEXT("TowerTrace"));
		FCollisionQueryParams TraceParams(TowerTag, true, Instigator);
		TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = true;
		
		// This fires the ray and checks against all objects w/ collision
		// pew pew pew
		FHitResult Hit(ForceInit);
		GetWorld()->LineTraceSingle(Hit, startPos, targetPos, TraceParams, FCollisionObjectQueryParams::AllObjects);
		// Did this hit anything?
		if (Hit.bBlockingHit)
		{

			// we've been hit captin!!!
			
				



				
			}
		}
	}*/
}
// old code ew

// now with this time, we calculate how far the 
// (fireDirectionVector.Y - startPos.Y > 0) // fire in front plz
//{ // we don't need to fir in front nymore cuz we good at coding yes yes
//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("fireDirection is %d"), fireDirectionVector.Y));
//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("startPos is %d && firedirection > startPos"), startPos.Y));
/*DrawDebugLine(GetWorld(), startPos, SpotToAimInVector, FColor::Blue, true, 10.0f);
static FName TowerTag = FName(TEXT("TowerTrace"));
FCollisionQueryParams TraceParams(TowerTag, true, Instigator);*/

//}

void AAIBaseTowerController::setCurrentState(BaseTowerControllerState newState) {
	currentState = newState;
}

void AAIBaseTowerController::startAttack()
{
	if (this)
		GetWorldTimerManager().SetTimer(this, &AAIBaseTowerController::attack, attackRate, true);
}
void AAIBaseTowerController::stopAttack()
{
	if (this)
		GetWorldTimerManager().ClearTimer(this, &AAIBaseTowerController::attack);
}

FVector AAIBaseTowerController::getVectorToAimIn(APawn* enemy)
{

	// http://www.gamasutra.com/blogs/KainShin/20090515/83954/Predictive_Aim_Mathematics_for_AI_Targeting.php
	// use this, this is strategy #3
	// get the movement vector of the enemy
	// again, get rotation then convert to unit vector

	// the original fire vector works if the speed of the bullet is 0 or super fast
	// but its not
	
	// we know the enemy speed and direction
	// we can also get the distance to enemy
	// folo the vector lines. ours plus theirs gives a point to aim in
	

	// we need to account for speed of enemy and our bullet speed

				/*  *
					\ \
					 \  \
	original aim	  \   \  <- the vector we want (direction -> and down)
	vector ->		   \    \
	direction down	    \_____\   <- enemy vector
SpotToAimInVectorStarts ^	  ^ the spot we are aiming for



	this code calculates based upon the enemies current position

	*/
	// first, get variables
	

	FVector bulletInitialPosition = myTower->cameraComponent->GetComponentLocation() + myTower->projectileOffset;
	FVector enemyInitialPosition = enemy->GetActorLocation();
	float enemySpeed = enemy->GetActorForwardVector().X;
	float bulletSpeed = ATowerProjectile::PROJECTILE_SPEED;
	FVector enemyDirection = enemy->GetViewRotation().Vector();
	FVector enemyVelocity = enemyDirection*enemySpeed;


	// we want to get bulletVelocity
	// this will get us time

	// speed is different from velocity!!!!!
	// velocity is speed in aim direction


	//firstEquation is
	//Length(enemyInitialPosition + EnemySpeed * time - bulletInitialPosition) = bulletSpeed * time
	// secondEquation is
	// bulletInitialPosition + bulletVelocity*time = enemyInitialPosition + enemyVelocity*time

	// solving for bullet Velocity and time

	// Law of cosines = a^2 + b^2 + 2*a*b*cos(theta) = c^2


	// this is also referred to as distance
	float a = (enemyInitialPosition - bulletInitialPosition).Size();


	// b = Length(enemyVelocity*time)
	// b = Length(enemyVelocity)*time
	// Length(enemyVelocity) = speed
	// b = enemySpeed*time
	// don't know time yet :/

	// c = bulletSpeed*time
	// again, don't know time yet


	float cosTheta = FVector::DotProduct((bulletInitialPosition - enemyInitialPosition).SafeNormal(1.0f), (enemyVelocity).SafeNormal(1.0f));


	// so, plug these into law of cosines

	// then reduce

	// you end up with

	// 0 = (bulletSpeed - enemySpeed)^2*time^2 + 2 * a * enemySpeed * cosTheta * t - a^2

	// quadratic now!! solve for t

	float x = FGenericPlatformMath::Pow(bulletSpeed - enemySpeed, 2);
	float y = 2 * a* enemySpeed *cosTheta;
	float z = -FGenericPlatformMath::Pow(a, 2);


	// reduced quadratic
	float time1 = (-y + FGenericPlatformMath::Pow(FGenericPlatformMath::Pow(y,2) - 4 * x * z, 0.5)) / (2 * x);
	float time2 = (-y - FGenericPlatformMath::Pow(FGenericPlatformMath::Pow(y, 2) - 4 * x * z, 0.5)) / (2 * x);
	bool doTime1 = false;
	bool doTime2 = false;
	if (time1 < 0)
	{
		doTime2 = true;
	}
	else if (time2 < 0) // only one will be < 0, if one is
	{
		doTime1 = true;
	}
	else if (time1 < time2)
	{
		doTime1 = true;
	}
	else if (time2 < time1)
	{
		doTime2 = true;
	}

	// always do the fast time
	// either will hit, but we want the faster time because we can
	float time = 0.0f;
	if (doTime1)
	{
		time = time1;
	}
	else if (doTime2)
	{
		time = time2;
	}
	else
	{

		// neither time works wut makes no sense
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Neither time works wut")));
	}
	
	
	// we now have time

	// now get velocity of bullet
	FVector bulletVelocity = enemyVelocity + ((enemyInitialPosition - bulletInitialPosition) / time);

	return bulletVelocity;



	// old code works but dusnt :/
	/*for (int i = 0; i < 1000; i++)
	{

		FVector enemyDirection = enemy->GetActorRotation().Vector();

		float projectileDistance = SpotToAimInVector.Size();

		float time = projectileDistance / projectileSpeed;

		float enemySpeed = enemy->GetActorForwardVector().X;

		float enemyDistance = time * enemySpeed;

		SpotToAimInVector = SpotToAimInVector + enemyDistance*enemyDirection;
	}
	


	// however, this is based upon our original aim vector!!!
	// we don't actually get the full time it takes, because we cant use the vector we want
	// so we have to repeat it

	// i think. i might be lying but it works so its chill

	return SpotToAimInVector;*/
}