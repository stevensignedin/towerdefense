// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "TowerProjectile.h"
#include "EnemyCharacter.h"


ATowerProjectile::ATowerProjectile(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    
    //  Use sphere for collision
	collisionComp = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
    collisionComp->InitSphereRadius(5.0f);
    collisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	collisionComp->OnComponentBeginOverlap.AddDynamic(this, &ATowerProjectile::onActorBeginOverlap);

    RootComponent = collisionComp;
	RootComponent->bShouldUpdatePhysicsVolume = false;
    
    //  Use ProjectileMovementComponent to govern this projectile's movement
    projectileMovement = PCIP.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
    projectileMovement->UpdatedComponent = collisionComp;
    projectileMovement->InitialSpeed = PROJECTILE_SPEED;
	projectileMovement->MaxSpeed = PROJECTILE_SPEED;
    projectileMovement->bRotationFollowsVelocity = true;
    projectileMovement->bShouldBounce = false;
	projectileMovement->Bounciness = 0.0f;  
    
    //  Die after 3 seconds
    this->InitialLifeSpan = 2.3f;
	bulletDamage = 15.0f;
}

void ATowerProjectile::OnBounce(FHitResult const& HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(this, HitEffect, this->GetActorLocation());
	SetActorHiddenInGame(true);
	projectileMovement->StopMovementImmediately();
	// give clients some time to show explosion
	SetLifeSpan(1.0f);
}

void ATowerProjectile::PostSpawnInitialize(FVector const& SpawnLocation, FRotator const& SpawnRotation, AActor* InOwner, APawn* InInstigator, bool bRemoteOwned, bool bNoFail, bool bDeferConstruction) 
{
	Super::PostSpawnInitialize(SpawnLocation, SpawnRotation, InOwner, InInstigator, bRemoteOwned, bNoFail, bDeferConstruction); 
	GetWorldTimerManager().SetTimer(this, &ATowerProjectile::beginLifeTime, InitialLifeSpan, false, 0); 
}

void ATowerProjectile::beginLifeTime()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, HitEffect, this->GetActorLocation());
	SetActorHiddenInGame(true);
	projectileMovement->StopMovementImmediately();
}
void ATowerProjectile::onActorBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{

	//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("In On Hit!!!")));
	//  Only add impulse and destroy projectile if we hit a physics
	//if(otherActor != NULL && otherActor != this && otherComp != NULL && otherComp->IsSimulatingPhysics()) {
	//    otherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

		// UGameplayStatics::SpawnEmitterAtLocation(this, HitEffect, Hit.ImpactPoint);
		UGameplayStatics::SpawnEmitterAtLocation(this, HitEffect, this->GetActorLocation());
		AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(OtherActor);
		if (Enemy)
		{
			// 2.0f is damage dealt
			Enemy->TakeDamage(bulletDamage, FDamageEvent(), GetInstigatorController(), this);

			//	Play impact sound
			TArray<AActor*> actors;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASoundBox::StaticClass(), actors);
			if (actors.Num() > 0) {
				((ASoundBox*)actors[0])->playImpactFX(this);
			}
		}
		SetActorHiddenInGame(true);
		projectileMovement->StopMovementImmediately();
		// give clients some time to show explosion
		SetLifeSpan(1.0f);
	//}
	
}
