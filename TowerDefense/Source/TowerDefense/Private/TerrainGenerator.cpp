// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "TerrainGenerator.h"


ATerrainGenerator::ATerrainGenerator(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	body = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("generateTerrain"));
	RootComponent = body;
}


void ATerrainGenerator::BeginPlay()
{
	int randIndex;
	for (int i = 0; i < 8; i++)
	{
		randIndex = FMath::RandRange(0, SpawnPoints.Num() - 1); //Generate random spawn point choice
		if (SpawnPoints.Num() > 0)
		{
			target = SpawnPoints[randIndex];
		}
		if (CharacterToSpawn != NULL && target)
		{
			if (UWorld* World = GetWorld())
			{
				FActorSpawnParameters SpawnParams;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = Instigator;
				AActor* const Char = World->SpawnActor<AActor>(CharacterToSpawn, target->GetActorLocation(), FRotator::ZeroRotator, SpawnParams);
			}
		}
	}
}
