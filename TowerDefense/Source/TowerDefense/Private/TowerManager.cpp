// Fill out your copyright notice in the Description page of Project Settings.
#include "TowerDefense.h"
#include "TowerManager.h"
ATowerManager::ATowerManager(const class FPostConstructInitializeProperties& PCIP)
: Super(PCIP)
{
	body = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("Body"));
	RootComponent = body;
	towerID = 1;
}

void ATowerManager::BeginPlay() {

	//  Create all towers
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	spawnParams.Instigator = Instigator;


	FRotator Rotation(0.0f, 0.0f, 0.0f);

	//  Go through all target points
	int32 size = targetPoints.Num();
	for (int32 i = 0; i < size; i++) {
		ATargetPoint* point = targetPoints[i];
		if (towerToSpawn) {
			ABaseTower* const tower = GetWorld()->SpawnActor<ABaseTower>(towerToSpawn, point->GetActorLocation(), Rotation, spawnParams);
			allTowers.Add(tower); //add the tower to the persistent array of towers
			//tower->setManager(this); 
		}
	}
}


ABaseTower* ATowerManager::Next()
{
	int32 size = allTowers.Num();
	for (int i = 0; i < size; i++)
	{
		if (allTowers[i]->isOccupied()) //find the tower you are in, since the only tower whose status is occupied should be the current one
		{
			allTowers[i]->exit(); 
			return allTowers[(i + 1) % size];
		}
	}
	return NULL; 
}

ABaseTower* ATowerManager::Previous()
{
	int32 size = allTowers.Num();
	for(int i = 0; i < size; i++)
	{
		if(allTowers[i]->isOccupied()) //find the tower you are in, since the only tower whose status is occupied should be the current one
		{
			allTowers[i]->exit();
			return allTowers[(i + size - 1) % size];
		}
	}
	return NULL;
}

void ATowerManager::updateTowerList()
{
	int32 size = allTowers.Num();
	TArray<ABaseTower*> removeThese; 
	for(int i = 0; i < size; i++)
	{
		if(!allTowers[i]->alive || allTowers[i]->IsPendingKill()) 
		{
			removeThese.Add(allTowers[i]);
		}
	}
	for(int j = 0; j < removeThese.Num(); j++)
	{
		allTowers.Remove(removeThese[j]); 
	}
}
void ATowerManager::spawnTower(FVector location) {
	if (towerToSpawn) {
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		FRotator Rotation(0.0f, 0.0f, 0.0f);

		ABaseTower* const tower = GetWorld()->SpawnActor<ABaseTower>(towerToSpawn, location, Rotation, spawnParams);
		tower->SpawnDefaultController();
		tower->spawnHealthBar();
		tower->ID = towerID;
		towerID++;
		allTowers.Add(tower);
	}
}

bool ATowerManager::markUnreachable(ABaseTower* tower) {
	for (int i = 0; i < allTowers.Num(); i++) {
		if (allTowers[i] == tower) {
			allTowers[i]->reachable = false;
			return true;
		}
	}
	return false;
}