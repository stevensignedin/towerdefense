// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "BuildModeCamera.h"


ABuildModeCamera::ABuildModeCamera(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	body = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("Body"));
	RootComponent = body;

	cameraComponent = PCIP.CreateDefaultSubobject<UCameraComponent>(this, TEXT("BuildCamera"));
	cameraComponent->AttachTo(RootComponent);
	cameraComponent->bUsePawnControlRotation = true;

	panSpeed = 1000.0f;
	zoomSpeed = 100.0f;
}


