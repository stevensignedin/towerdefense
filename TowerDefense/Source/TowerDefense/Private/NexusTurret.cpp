// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "NexusTurret.h"


ANexusTurret::ANexusTurret(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	nexusMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("NexusMesh"));
	RootComponent = nexusMesh;
	Health = 1000; 
	//bCastDynamicShadow = false;
}

float ANexusTurret::getHealth()
{
	return Health;
}

float ANexusTurret::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
		EventInstigator, DamageCauser);

	if (ActualDamage > 0.0f)
	{
		 Health -= ActualDamage;
		if (Health <= 0.0f)
		{
			// We're dead
			bCanBeDamaged = false; // Don't allow further damage
			Destroy();

			//	Call endgame
			ATowerDefensePlayerController* MyPlayerController = (ATowerDefensePlayerController*)UGameplayStatics::GetPlayerController(GetWorld(), 0);
			MyPlayerController->makeFinished();
		}
	}
	return ActualDamage;
}
