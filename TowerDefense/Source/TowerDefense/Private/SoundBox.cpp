// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "SoundBox.h"


ASoundBox::ASoundBox(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	body = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("Body"));
	RootComponent = body;
}

UAudioComponent* ASoundBox::playSound(USoundCue* sound) {
	UAudioComponent* ac = NULL;

	if (sound) {
		ac = UGameplayStatics::PlaySoundAttached(sound, RootComponent);
	}
	return ac;
}

UAudioComponent* ASoundBox::playSound(USoundCue* sound, AActor* actorToPlayFrom) {
	UAudioComponent* ac = NULL;

	if (sound) {
		ac = UGameplayStatics::PlaySoundAttached(sound, actorToPlayFrom->GetRootComponent());
	}
	return ac;
}

UAudioComponent* ASoundBox::playSound(USoundCue* sound, AActor* actorToPlayFrom, bool stopWhenKilled) {
	UAudioComponent* ac = NULL;

	if (sound) {
		ac = UGameplayStatics::PlaySoundAttached(sound, actorToPlayFrom->GetRootComponent(), FName("Fire"),
			actorToPlayFrom->GetActorLocation(), EAttachLocation::Type::KeepWorldPosition, stopWhenKilled);
	}
	return ac;
}

void ASoundBox::playBackgroundMusic() {
	if (backgroundMusic)
		playSound(backgroundMusic);
}

void ASoundBox::playImpactFX(AActor* hit) {
	if (impactFX)
		playSound(impactFX, hit);
}

void ASoundBox::playFireFX(AActor* hit) {
	if (fireFX)
		playSound(fireFX, hit, true);
}

void ASoundBox::playZombieFX() {
	if (zombieFX)
		playSound(zombieFX);
}