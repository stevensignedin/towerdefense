// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "TowerDefense.h"
#include "TowerDefenseGameMode.h"
#include "TowerDefensePlayerController.h"
#include "TowerDefenseCharacter.h"
#include "TowerDefenseHUD.h"

ATowerDefenseGameMode::ATowerDefenseGameMode(const class FPostConstructInitializeProperties& PCIP) : Super(PCIP)
{
	// use our custom PlayerController class
	PlayerControllerClass = ATowerDefensePlayerController::StaticClass();

	
	// set default pawn class to our Blueprinted character
	// WHY DO WE NEED THIS isnt this making our blue dude im killing the mofo
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/MyCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
	 	DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	HUDClass = ATowerDefenseHUD::StaticClass();
}